# Contributing

* Please lint your code using the black formatter
* Docstrings are formatted using the [numpy docstring format](https://numpydoc.readthedocs.io/en/latest/format.html#docstring-standard)
* Versioning is done using a major.devN scheme 
