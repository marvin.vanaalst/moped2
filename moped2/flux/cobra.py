from __future__ import annotations

from typing import cast

import cobra
import pandas as pd

from ..core.model import Model


def get_influx_reactions(
    model: Model, cobra_solution: cobra.Solution, sort_result: bool = False
) -> pd.DataFrame:
    """Get influxes from a cobra simulation."""
    exchange_reactions = {i for i in model.reactions if i.startswith("EX_")}
    exchange_fluxes = cobra_solution.to_frame().loc[exchange_reactions, "fluxes"]  # type: ignore
    result = exchange_fluxes[exchange_fluxes > 0]
    if sort_result:
        return result.sort_values(ascending=False)
    return result


def get_efflux_reactions(
    model: Model, cobra_solution: cobra.Solution, sort_result: bool = False
) -> pd.DataFrame:
    """Get effluxes from a cobra simulation."""
    exchange_reactions = {i for i in model.reactions if i.startswith("EX_")}
    exchange_fluxes = cobra_solution.to_frame().loc[exchange_reactions, "fluxes"]  # type: ignore
    result = exchange_fluxes[exchange_fluxes < 0]
    if sort_result:
        return result.sort_values(ascending=True)
    return result


def get_producing_reactions(
    model: Model,
    cobra_solution: cobra.Solution,
    compound_id: str,
    cutoff: float = 0,
) -> dict[str, float]:
    """Get reactions that produce the compound in the cobra simulation."""
    producing = {}
    for reaction_id in model.compounds[compound_id].in_reaction:
        flux = cobra_solution[reaction_id] * model.reactions[reaction_id].stoichiometries[compound_id]
        if flux > cutoff:
            producing[reaction_id] = flux
    return producing


def get_consuming_reactions(
    model: Model,
    cobra_solution: cobra.Solution,
    compound_id: str,
    cutoff: float = 0,
) -> dict[str, float]:
    """Get reactions that consume the compound in the cobra simulation."""
    consuming = {}
    for reaction_id in model.compounds[compound_id].in_reaction:
        flux = -cobra_solution[reaction_id] * model.reactions[reaction_id].stoichiometries[compound_id]
        if flux > cutoff:
            consuming[reaction_id] = flux
    return consuming


def to_cobra(model: Model) -> cobra.Model:
    """Export the model into a cobra model to do FBA topological."""
    cobra_model = cobra.Model(model.name)
    cobra_model.compartments = {v: k for k, v in model.compartments.items()}
    cobra_model.add_metabolites(
        [
            cobra.Metabolite(
                id=cpd.id,
                formula=cpd.formula_to_string(),
                charge=cpd.charge,
                compartment=model.compartments[cast(str, cpd.compartment)],
            )
            for cpd in model.compounds.values()
        ]
    )

    for rxn in model.reactions.values():
        c_rxn = cobra.Reaction(id=rxn.id)
        cobra_model.add_reaction(c_rxn)
        c_rxn.bounds = rxn.bounds if rxn.bounds else (0, 1000)
        c_rxn.add_metabolites(rxn.stoichiometries)

    if model.objective is not None:
        cobra_model.objective = {cobra_model.reactions.get_by_id(k): v for k, v in model.objective.items()}
    return cobra_model
