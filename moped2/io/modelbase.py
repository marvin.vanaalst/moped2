from __future__ import annotations

import warnings

from ..core.model import Model

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import modelbase.ode as ode
    from modelbase.ode import ratelaws as rl


def _add_modelbase_influx_reaction(
    mod: ode.Model,
    rxn_id: str,
    metabolite: list[str],
    ratelaw: str = "constant",
    suffix: str | None = None,
) -> None:
    if ratelaw == "constant":
        k_in = f"k_in_{rxn_id}"
        mod.add_parameters({k_in: 1})
        if suffix is not None:
            rxn_id += f"_{suffix}"
        mod.add_reaction_from_ratelaw(
            rate_name=rxn_id,
            ratelaw=rl.Constant(product=metabolite[0], k=k_in),
        )
    else:
        raise NotImplementedError


def _add_modelbase_efflux_reaction(
    mod: ode.Model,
    rxn_id: str,
    metabolite: list[str],
    ratelaw: str = "mass-action",
    suffix: str | None = None,
) -> None:
    if ratelaw == "mass-action":
        k_out = f"k_out_{rxn_id}"
        mod.add_parameters({k_out: 1})
        if suffix is not None:
            rxn_id += f"_{suffix}"
        mod.add_reaction_from_ratelaw(
            rate_name=rxn_id,
            ratelaw=rl.MassAction(substrates=metabolite, products=[], k_fwd=k_out),
        )
    else:
        raise NotImplementedError


def _add_modelbase_medium_reaction(
    mod: ode.Model,
    rxn_id: str,
    metabolite: list[str],
    influx_ratelaw: str = "constant",
    efflux_ratelaw: str = "mass-action",
) -> None:
    _add_modelbase_influx_reaction(
        mod=mod,
        rxn_id=rxn_id,
        metabolite=metabolite,
        ratelaw=influx_ratelaw,
        suffix="in",
    )
    _add_modelbase_efflux_reaction(
        mod=mod,
        rxn_id=rxn_id,
        metabolite=metabolite,
        ratelaw=efflux_ratelaw,
        suffix="out",
    )


def _add_modelbase_irreversible_reaction(
    mod: ode.Model,
    rxn_id: str,
    substrates: list[str],
    products: list[str],
    ratelaw: str = "mass-action",
) -> None:
    if ratelaw == "mass-action":
        k_fwd = f"k_{rxn_id}"
        mod.add_parameters({k_fwd: 1})
        mod.add_reaction_from_ratelaw(
            rate_name=rxn_id,
            ratelaw=rl.MassAction(substrates=substrates, products=products, k_fwd=k_fwd),
        )
    else:
        raise NotImplementedError


def _add_modelbase_reversible_reaction(
    mod: ode.Model,
    rxn_id: str,
    substrates: list[str],
    products: list[str],
    ratelaw: str = "mass-action",
) -> None:
    if ratelaw == "mass-action":
        k_fwd = f"kf_{rxn_id}"
        k_bwd = f"kr_{rxn_id}"
        mod.add_parameters({k_fwd: 1, k_bwd: 1})
        mod.add_reaction_from_ratelaw(
            rate_name=rxn_id,
            ratelaw=rl.ReversibleMassAction(
                substrates=substrates,
                products=products,
                k_fwd=k_fwd,
                k_bwd=k_bwd,
            ),
        )
    else:
        raise NotImplementedError


def _stoich_dict_to_list(stoich_dict: dict[str, float], reaction: str) -> list[str]:
    stoich_list = []
    for cpd, stoich in stoich_dict.items():
        if not stoich == int(stoich):
            warnings.warn(f"Check stoichiometries for reaction {reaction}, possible integer rounddown.")
        stoich_list.extend([cpd] * max(int(abs(stoich)), 1))
    return stoich_list


def to_kinetic_model(
    model: Model,
    reaction_ratelaw: str = "mass-action",
    influx_ratelaw: str = "constant",
    efflux_ratelaw: str = "mass-action",
) -> ode.Model:
    """Convert the model into a kinetic modelbase model."""
    mod = ode.Model()
    mod.add_compounds(sorted(model.compounds))

    for rxn_id, reaction in sorted(model.reactions.items()):
        _substrates, _products = reaction.split_stoichiometries()
        substrates = _stoich_dict_to_list(_substrates, reaction=rxn_id)
        products = _stoich_dict_to_list(_products, reaction=rxn_id)

        if len(reaction.stoichiometries) == 1:
            if reaction.reversible:
                _add_modelbase_medium_reaction(
                    mod=mod,
                    rxn_id=rxn_id,
                    metabolite=substrates,
                    influx_ratelaw=influx_ratelaw,
                    efflux_ratelaw=efflux_ratelaw,
                )
            else:
                if reaction.bounds is not None and reaction.bounds[0] < 0:
                    _add_modelbase_influx_reaction(
                        mod=mod,
                        rxn_id=rxn_id,
                        metabolite=substrates,
                        ratelaw=influx_ratelaw,
                    )
                else:
                    _add_modelbase_efflux_reaction(
                        mod=mod,
                        rxn_id=rxn_id,
                        metabolite=substrates,
                        ratelaw=efflux_ratelaw,
                    )
        else:
            if reaction.reversible:
                _add_modelbase_reversible_reaction(
                    mod=mod,
                    rxn_id=rxn_id,
                    substrates=substrates,
                    products=products,
                    ratelaw=reaction_ratelaw,
                )
            else:
                _add_modelbase_irreversible_reaction(
                    mod=mod,
                    rxn_id=rxn_id,
                    substrates=substrates,
                    products=products,
                    ratelaw=reaction_ratelaw,
                )
    return mod


def to_kinetic_model_source_code(
    model: Model,
    reaction_ratelaw: str = "mass-action",
    influx_ratelaw: str = "constant",
    efflux_ratelaw: str = "mass-action",
) -> str:
    """Convert the model into modelbase model soure code."""
    mod = to_kinetic_model(
        model=model,
        reaction_ratelaw=reaction_ratelaw,
        influx_ratelaw=influx_ratelaw,
        efflux_ratelaw=efflux_ratelaw,
    )
    return mod.generate_model_source_code()  # type: ignore
