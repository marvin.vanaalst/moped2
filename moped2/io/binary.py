from __future__ import annotations

import pickle
from pathlib import Path

from ..core.model import Model


def to_pickle(model: Model, filename: str | Path) -> None:
    with open(filename, "wb") as file:
        pickle.dump(model, file)


def load_model_from_pickle(filename: str | Path) -> Model:
    with open(filename, "rb") as file:
        model = pickle.load(file)
    if isinstance(model, Model):
        return model
    raise NotImplementedError("Pickled object is not a Model")
