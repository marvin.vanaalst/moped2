from __future__ import annotations

import json
from functools import singledispatch
from pathlib import Path
from typing import Any

import yaml

from ..core.compound import Compound
from ..core.model import Model
from ..core.monomer import Monomer
from ..core.reaction import Reaction
from ..core.kinetic_data import KineticData


@singledispatch
def serialize(x: Any) -> Any:
    raise NotImplementedError(x.__class__.__name__)


@serialize.register
def _serialize_none(x: None) -> None:
    return None


@serialize.register
def _serialize_str(x: str) -> str:
    return x


@serialize.register
def _serialize_int(x: int) -> str:
    return str(x)


@serialize.register
def _serialize_float(x: float) -> str:
    return str(x)


@serialize.register
def _serialize_set(x: set) -> list[str]:
    return [serialize(i) for i in x]


@serialize.register
def _serialize_list(x: list) -> list[str]:
    return [serialize(i) for i in x]


@serialize.register
def _serialize_tuple(x: tuple) -> list[str]:
    return [serialize(i) for i in x]


@serialize.register
def _serialize_dict(x: dict) -> dict[str, Any]:
    return {k: serialize(v) for k, v in x.items()}


@serialize.register
def _serialize_model(x: Model) -> dict[str, Any]:
    return {
        "compounds": serialize(list(x.compounds.values())),
        "reactions": serialize(list(x.reactions.values())),
        "compartments": serialize(x.compartments),
        "objective": serialize(x.objective),
        "minimal_seed": serialize(x.minimal_seed),
        "name": serialize(x.name),
        "base_cofactor_pairs": serialize(x._base_cofactor_pairs),
    }


@serialize.register
def _serialize_compound(x: Compound) -> dict[str, Any]:
    return {k: serialize(v) for k, v in x.__dict__.items()}


@serialize.register
def _serialize_reaction(x: Reaction) -> dict[str, Any]:
    return {k: serialize(v) for k, v in x.__dict__.items()}


def _deserialize_compounds(obj: dict[str, Any]) -> list[Compound]:
    return [
        Compound(
            base_id=str(i["base_id"]),
            id=str(i["id"]),
            name=str(i["name"]),
            compartment=str(i["compartment"]),
            smiles=str(i["smiles"]),
            formula={str(k): float(v) for k, v in i["formula"].items()},
            charge=int(charge) if (charge := i["charge"]) is not None else None,
            gibbs0=float(gibbs0) if (gibbs0 := i["gibbs0"]) is not None else None,
            database_links={str(k): set(v) for k, v in i["database_links"].items()},
            types=list(i["types"]),
            in_reaction=set(i["in_reaction"]),
        )
        for i in obj["compounds"]
    ]


def _deserialize_reactions(obj: dict[str, Any]) -> list[Reaction]:
    return [
        Reaction(
            base_id=str(i["base_id"]),
            id=str(i["id"]),
            name=str(name) if (name := i["name"]) is not None else None,
            compartment=str(comp)
            if isinstance(comp := i["compartment"], str)
            else tuple(str(x) for x in comp),
            bounds=(float(i["bounds"][0]), float(i["bounds"][1])),
            gibbs0=float(gibbs0) if (gibbs0 := i["gibbs0"]) is not None else None,
            ec=i["ec"],
            transmembrane=i["transmembrane"] == "True",
            types=list(i["types"]),
            pathways=set(i["pathways"]),
            stoichiometries={k: float(v) for k, v in i["stoichiometries"].items()},
            sequences={k: v for k, v in i["sequences"].items()},
            monomers={k: {k2: Monomer(**v2)} for k, v in i["monomers"].items() for k2, v2 in v.items()},
            kinetic_data={k: KineticData(**v) for k, v in i["kinetic_data"].items()},
            database_links={k: set(v) for k, v in i["database_links"].items()},
        )
        for i in obj["reactions"]
    ]


def _deserialize(obj: dict[str, Any]) -> Model:
    return Model(
        compounds=_deserialize_compounds(obj),
        reactions=_deserialize_reactions(obj),
        compartments=obj["compartments"],
        objective=obj["objective"],
        minimal_seed=set(obj["minimal_seed"]),
        name=obj["name"],
        cofactor_pairs=obj["base_cofactor_pairs"],
    )


def to_json(model: Model, filename: str | Path) -> None:
    with open(filename, "w") as file:
        json.dump(serialize(model), file, indent=2)


def to_yaml(model: Model, filename: str | Path) -> None:
    with open(filename, "w") as file:
        yaml.dump(serialize(model), file)


def load_model_from_json(filename: str | Path) -> "Model":
    with open(filename, "r") as file:
        obj = json.load(file)
    return _deserialize(obj)


def load_model_from_yaml(filename: str | Path) -> "Model":
    with open(filename, "r") as file:
        obj = yaml.safe_load(file)
    return _deserialize(obj)
