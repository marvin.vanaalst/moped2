from __future__ import annotations

from pathlib import Path
from typing import Union, Optional

from ..core.model import Model
from .sbml import load_model_from_sbml

COFACTOR_PAIRS = {
    "atp": "adp",
    "gtp": "gdp",
    "nadh": "nad",
    "nadph": "nadp",
    "10fthf": "thf",
    "methf": "thf",
    "fdxrd": "fdxox",
    "trdrd": "trdox",
    "etfrd": "etfox",
    "accoa": "coa",
    "pcrd": "pcox",
}


def load_model_from_bigg(
    bigg_sbml_file: Union[str, Path],
    cofactor_pairs: Optional[dict[str, str]] = None,
) -> Model:
    if cofactor_pairs is None:
        cofactor_pairs = COFACTOR_PAIRS

    model = load_model_from_sbml(sbml_file=bigg_sbml_file)

    for strong_cofactor_base_id, weak_cofactor_base_id in cofactor_pairs.items():
        model.add_cofactor_pair(
            strong_cofactor_base_id=strong_cofactor_base_id,
            weak_cofactor_base_id=weak_cofactor_base_id,
        )
    return model
