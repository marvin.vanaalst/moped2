from __future__ import annotations

import re

import cobra

from ..core.compound import Compound
from ..core.model import Model
from ..core.reaction import Reaction
from ..utils import strip_compartment_suffix


def load_model_from_cobra(cobra_model: cobra.Model) -> Model:
    """Import a cobra model into this model."""
    model = Model()
    compartment_map = cobra_model.compartments
    model.compartments.update({v: k for k, v in compartment_map.items()})
    compartment_suffixes = re.compile("|".join(set([f"(_{i}$)" for i in compartment_map.keys()])))

    objective = {}
    for metabolite in cobra_model.metabolites:
        base_id = strip_compartment_suffix(
            object_id=metabolite.id,
            compartment_pattern=compartment_suffixes,
        )
        compartment = compartment_map[metabolite.compartment]
        model.add_compound(
            Compound(
                base_id=base_id,
                id=metabolite.id,
                name=metabolite.name,
                compartment=compartment,
                formula=metabolite.elements,
                charge=metabolite.charge,
                gibbs0=None,
            )
        )
    for reaction in cobra_model.reactions:
        obj_coef = reaction.objective_coefficient
        if obj_coef != 0:
            objective[reaction.id] = obj_coef

        model.add_reaction(
            Reaction(
                base_id=strip_compartment_suffix(
                    object_id=reaction.id,
                    compartment_pattern=compartment_suffixes,
                ),
                id=reaction.id,
                stoichiometries={k.id: v for k, v in reaction.metabolites.items()},
                bounds=reaction.bounds,
                name=reaction.name,
            )
        )
    model.set_objective(objective=objective)
    return model
