import logging
import os

from . import core, databases, flux, io, topological, utils
from .core.compound import Compound
from .core.model import Model
from .core.reaction import Reaction
from .databases.cyc import create_model_from_pgdb
from .flux.cobra import (
    get_consuming_reactions,
    get_efflux_reactions,
    get_influx_reactions,
    get_producing_reactions,
    to_cobra,
)
from .io.bigg import load_model_from_bigg
from .io.binary import load_model_from_pickle, to_pickle
from .io.cobra import load_model_from_cobra
from .io.modelbase import to_kinetic_model
from .io.sbml import load_model_from_sbml, to_sbml
from .io.serde import load_model_from_json, load_model_from_yaml, to_json, to_yaml
from .sequence.blast import (
    blast_is_installed,
    create_submodel_from_genome,
    create_submodel_from_proteome,
)
from .topological import to_topological
from .topological.gapfilling import get_gapfilling_reactions
from .topological.scope import multiple_scopes, scope, scope_by_iteration
from .topological.treesearch import metabolite_tree_search
from .utils import get_biomass_template

logger = logging.getLogger("moped")
logger.setLevel(logging.WARNING)
formatter = logging.Formatter(
    fmt="{asctime} - {levelname} - {message}",
    datefmt="%Y-%m-%d %H:%M:%S",
    style="{",
)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
logger.addHandler(handler)

os.environ["LC_ALL"] = "C"  # meneco bugfix
