from __future__ import annotations

from dataclasses import dataclass, field


@dataclass
class TopologicalReaction:
    substrates: set[str]
    products: set[str]


@dataclass
class TopologicalCompound:
    in_reaction: set[str] = field(default_factory=set)


@dataclass
class TopologicalModel:
    reactions: dict[str, TopologicalReaction]
    compounds: dict[str, TopologicalCompound]
