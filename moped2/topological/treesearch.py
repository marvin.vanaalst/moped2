from __future__ import annotations

from dataclasses import dataclass
from queue import LifoQueue, Queue
from typing import cast

from typing_extensions import Literal

from .data import TopologicalModel


@dataclass
class SearchResult:
    reactions: list[str]
    compounds: list[str]


def _deconstruct_path(end: str, parents: dict[str, tuple[str | None, str | None]]) -> SearchResult:
    """Deconstruct the path taken."""
    met, rec = parents[end]
    mets = [met]
    recs = [rec]
    while True:
        met, rec = parents[cast(str, met)]
        if met is not None:
            recs.append(rec)
            mets.append(met)
        else:
            return SearchResult(
                reactions=cast(list[str], recs[::-1]),
                compounds=cast(list[str], mets[::-1] + [end]),
            )


def _add_metabolite(
    child_metabolite: str,
    parent_metabolite: str,
    reaction_id: str,
    parents: dict[str, tuple[str | None, str | None]],
    Q: LifoQueue | Queue,
) -> None:
    if child_metabolite not in parents:
        parents[child_metabolite] = parent_metabolite, reaction_id
        Q.put(child_metabolite)


def metabolite_tree_search(
    model: TopologicalModel,
    start_compound_id: str,
    end_compound_id: str,
    cofactors: set[str],
    max_iterations: int,
    search_type: Literal["breadth-first", "depth-first"],
) -> SearchResult:
    """Do a tree search to find the shortest connection between two compounds."""

    if search_type == "breadth-first":
        Q: Queue | LifoQueue = Queue()  # FIFO Queue
    elif search_type == "depth-first":
        Q = LifoQueue()
    else:
        raise ValueError("Unknown search type, choose from {breadth-first, depth-first}")

    Q.put(start_compound_id)
    # Parent metabolites and reactions of discovered metabolites
    parents: dict[str, tuple[str | None, str | None]] = {
        k: (None, None) for k in (cofactors ^ {start_compound_id})
    }
    n = 0

    compounds = model.compounds
    reactions = model.reactions

    while not Q.empty():
        substrate_id = Q.get()
        if substrate_id == end_compound_id:
            return _deconstruct_path(end_compound_id, parents)

        for reaction_id in compounds[substrate_id].in_reaction:
            reaction = reactions[reaction_id]
            if substrate_id in reaction.substrates:
                for product_id in reaction.products:
                    _add_metabolite(product_id, substrate_id, reaction_id, parents, Q)
        n += 1
        if n == max_iterations:
            raise ValueError("Exceeded max iterations")
    else:
        raise ValueError("Could not find a solution")
