from __future__ import annotations
from typing import DefaultDict

__all__ = [
    "gapfilling",
    "scope",
    "treesearch",
]

from collections import defaultdict
from ..core.model import Model
from . import gapfilling, scope, treesearch
from .data import TopologicalCompound, TopologicalModel, TopologicalReaction


def _split_stoichiometry(stoichiometry: dict[str, float]) -> tuple[set[str], set[str]]:
    substrates = set()
    products = set()
    for cpd, factor in stoichiometry.items():
        if factor > 0:
            products.add(cpd)
        else:
            substrates.add(cpd)
    return substrates, products


def to_topological(model: Model) -> TopologicalModel:
    new_reactions: dict[str, TopologicalReaction] = {}
    new_compounds: DefaultDict[str, TopologicalCompound] = defaultdict(TopologicalCompound)
    for reaction_id, reaction in model.reactions.items():
        stoichiometry = reaction.stoichiometries.copy()
        substrates, products = _split_stoichiometry(stoichiometry)
        new_reactions[reaction_id] = TopologicalReaction(substrates, products)
        if reaction.reversible:
            new_reactions[f"{reaction_id}__rev__"] = TopologicalReaction(products, substrates)

        # Cofactor duplication
        reaction_cofactors = []
        for cof, pair in model.cofactor_pairs.items():
            if (cs := reaction.stoichiometries.get(cof)) is None:
                continue
            if (ps := reaction.stoichiometries.get(pair)) is None:
                continue
            if cs == -ps:
                reaction_cofactors.append((cof, pair))
        if len(reaction_cofactors) > 0:
            for (cof, pair) in reaction_cofactors:
                stoichiometry[f"{cof}__cof__"] = stoichiometry.pop(cof)
                stoichiometry[f"{pair}__cof__"] = stoichiometry.pop(pair)
            new_id = f"{reaction_id}__cof__"
            substrates, products = _split_stoichiometry(stoichiometry)
            new_reactions[new_id] = TopologicalReaction(substrates, products)
            if reaction.reversible:
                new_reactions[f"{new_id}__rev__"] = TopologicalReaction(products, substrates)

    for (reaction_id, topo_reaction) in new_reactions.items():
        for substrate in topo_reaction.substrates:
            new_compounds[substrate].in_reaction.add(reaction_id)
        for product in topo_reaction.products:
            new_compounds[product].in_reaction.add(reaction_id)
    return TopologicalModel(new_reactions, new_compounds)
