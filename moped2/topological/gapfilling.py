from __future__ import annotations

from typing import Iterable, cast
import logging
import pyasp
from __meneco__ import query, utils

from .data import TopologicalModel

logger = logging.getLogger("moped")


def model_to_termset(model: TopologicalModel, model_name: str) -> pyasp.TermSet:
    model_terms = pyasp.TermSet()
    for reaction_id, reaction in model.reactions.items():
        model_terms.add(pyasp.Term("reaction", ['"' + reaction_id + '"', '"' + model_name + '"']))
        for substrate in reaction.substrates:
            model_terms.add(
                pyasp.Term(
                    "reactant",
                    [
                        '"' + substrate + '"',
                        '"' + reaction_id + '"',
                        '"' + model_name + '"',
                    ],
                )
            )
        for product in reaction.products:
            model_terms.add(
                pyasp.Term(
                    "product",
                    [
                        '"' + product + '"',
                        '"' + reaction_id + '"',
                        '"' + model_name + '"',
                    ],
                )
            )
    return model_terms


def compound_id_to_term(compound_type: str, compound_id: str) -> pyasp.Term:
    return pyasp.Term(compound_type, ['"' + compound_id + '"'])


def compound_list_to_termset(compound_type: str, compound_iterable: Iterable[str]) -> pyasp.TermSet:
    terms = pyasp.TermSet()
    for compound_id in compound_iterable:
        terms.add(compound_id_to_term(compound_type, compound_id))
    return terms


def term_to_str(term: pyasp.Term) -> str:
    return cast(str, term.arg(0)).strip('"').removesuffix("__rev__").removesuffix("__cof__")


def get_unproducible_compounds(
    model: pyasp.TermSet, targets: Iterable[str], seed: Iterable[str]
) -> pyasp.TermSet:
    return pyasp.TermSet(
        [
            compound_id_to_term("target", i.arg(0).strip('"'))
            for i in query.get_unproducible(model, seed, targets)
        ]
    )


def get_essential_reactions(
    model: pyasp.TermSet,
    database: pyasp.TermSet,
    seed: Iterable[str],
    producible: Iterable[str],
) -> pyasp.TermSet:
    """Get essential reactions to produce the producible reactions.

    Parameters
    ----------
    model: pyasp.term.TermSet
    database: pyasp.term.TermSet
        All possible reactions
    seed: Iterable(str)
        Compounds with which the algorithm starts
    producible: Iterable(str)
        Compounds that can be produced in the database

    Returns
    -------
    essential_reactions: pyasp.TermSet
        All reactions that are essential to produce the producible reactions
    """
    essential_reactions: pyasp.TermSet = pyasp.TermSet()
    for target in producible:
        single_target = pyasp.TermSet()
        single_target.add(target)
        essentials = query.get_intersection_of_completions(model, database, seed, single_target)
        essential_reactions = cast(pyasp.TermSet, essential_reactions.union(essentials))
    return essential_reactions


def get_gapfilling_reactions(
    model: TopologicalModel,
    reference_model: TopologicalModel,
    seed: Iterable[str],
    targets: Iterable[str],
    minimal_completion_size: bool = False,
    verbose: bool = False,
) -> list[str]:
    seed = set(seed)
    for target in targets:
        if target not in reference_model.compounds:
            logger.warn(f"Target {target} could not be found in the reference model")

    model_terms = model_to_termset(model, "query")
    reference_terms = model_to_termset(reference_model, "reference")
    seed = compound_list_to_termset("seed", seed)
    targets = compound_list_to_termset("target", targets)

    unproducible_model = get_unproducible_compounds(model_terms, targets, seed)
    if verbose:
        print(f"Searching for {[term_to_str(p) for p in unproducible_model]} in reference database")

    unproducible_database = get_unproducible_compounds(reference_terms, targets, seed)
    producible = unproducible_model.difference(unproducible_database)

    if len(unproducible_database) > 0:
        logger.warn(
            f"Could not produce {[term_to_str(p) for p in unproducible_database]} in reference database"
        )
        if verbose:
            print(f"Could produce {[term_to_str(p) for p in producible]} in reference database")
    else:
        if verbose:
            print("Could produce all compounds in reference database")

    essential_reactions = get_essential_reactions(model_terms, reference_terms, seed, producible)
    if verbose:
        print(f"Found {len(essential_reactions)} essential reaction(s)")
    filled_model = pyasp.TermSet(model_terms.union(essential_reactions))
    utils.clean_up()

    if not minimal_completion_size:
        return [term_to_str(term) for term in essential_reactions]

    # Get minimal solution
    if verbose:
        print("Searching for minimal set")
    min_models = query.get_minimal_completion_size(filled_model, reference_terms, seed, producible)
    return [term_to_str(term) for term in min_models[0]]
