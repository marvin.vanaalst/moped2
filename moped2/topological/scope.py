from __future__ import annotations

import sys
from collections.abc import Iterable
from concurrent import futures
from dataclasses import dataclass
from functools import reduce
from typing import Type, TypeVar

from tqdm.auto import tqdm

from .data import TopologicalModel


@dataclass
class ScopeResult:
    reactions: list[set[str]]
    compounds: list[set[str]]


@dataclass
class LumpedScopeResult:
    reactions: set[str]
    compounds: set[str]


T = TypeVar("T")


def _combine_sets(s1: set[T], s2: set[T]) -> set[T]:
    return s1 | s2


def _lump_results(result: ScopeResult) -> LumpedScopeResult:
    return LumpedScopeResult(
        reactions=reduce(_combine_sets, result.reactions),
        compounds=reduce(_combine_sets, result.compounds),
    )


def _scope(seed: Iterable[str], model: TopologicalModel) -> ScopeResult:
    all_compounds = set(seed)
    scope_reactions: list[set[str]] = []
    scope_compounds: list[set[str]] = []
    reactions = model.reactions
    possible_reactions = set(reactions.keys())
    while True:
        new_reactions: set[str] = set()
        new_compounds: set[str] = set()
        reactions_to_remove = set()
        for reaction in possible_reactions:
            if set(reactions[reaction].substrates).issubset(all_compounds):
                reactions_to_remove.add(reaction)
                new_reactions.add(reaction)
                new_compounds = new_compounds.union(set(reactions[reaction].products))
        if len(new_reactions) > 0:
            # Remove duplicate compounds
            new_compounds = new_compounds.difference(all_compounds)
            scope_reactions.append(new_reactions)
            scope_compounds.append(new_compounds)
            # Update iterables
            all_compounds = all_compounds.union(new_compounds)
            possible_reactions = possible_reactions.difference(reactions_to_remove)
        else:
            return ScopeResult(reactions=scope_reactions, compounds=scope_compounds)


def _scope_worker(seed: Iterable[str], model: TopologicalModel) -> tuple[tuple[str, ...], LumpedScopeResult]:
    return tuple(seed), _lump_results(_scope(model=model, seed=set(seed)))


def scope(model: TopologicalModel, seed: Iterable[str]) -> LumpedScopeResult:
    return _lump_results(_scope(seed=set(seed), model=model))


def scope_by_iteration(
    model: TopologicalModel,
    seed: Iterable[str],
) -> ScopeResult:
    return _scope(seed=set(seed), model=model)


def multiple_scopes(
    model: TopologicalModel,
    seeds: Iterable[Iterable[str]],
    multiprocessing: bool = True,
) -> dict[tuple[str, ...], LumpedScopeResult]:
    seeds_normalized = [tuple(i) for i in seeds]

    Executor: Type[futures.Executor]
    if not multiprocessing or sys.platform in ["win32", "cygwin"]:
        Executor = futures.ThreadPoolExecutor
    else:
        Executor = futures.ProcessPoolExecutor

    results = {}
    with tqdm(total=len(seeds_normalized)) as pbar:
        with Executor() as executor:
            for task in futures.as_completed(
                (executor.submit(_scope_worker, seed, model) for seed in seeds_normalized)
            ):
                seed, result = task.result()
                results[seed] = result
                pbar.update(1)
    return results
