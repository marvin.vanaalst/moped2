from __future__ import annotations

import copy
import dataclasses
from collections import defaultdict
from typing import Any, DefaultDict, Iterable, cast, Optional

import numpy as np
import pandas as pd

from .compound import Compound
from .reaction import Reaction


class Model:
    """The main model class."""

    def __init__(
        self,
        compounds: Iterable[Compound],
        reactions: Iterable[Reaction],
        compartments: dict[str, str],
        minimal_seed: set[str],
        cofactor_pairs: dict[str, str],
        objective: Optional[dict[str, float]],
        name: Optional[str],
    ) -> None:
        self.name: str = name if name is not None else "Model"
        self.compartments: dict[str, str] = {}
        self.compounds: dict[str, Compound] = {}
        self.base_compounds: dict[str, set[str]] = {}
        self.reactions: dict[str, Reaction] = {}
        self.base_reactions: dict[str, set[str]] = {}
        self.variant_reactions: dict[str, set[str]] = {}
        self.pathways: dict[str, set[str]] = {}
        self.objective: dict[str, float] = {}
        self.cofactor_pairs: dict[str, str] = dict()
        self.minimal_seed = set() if minimal_seed is None else minimal_seed
        # Filled by routines
        self._compound_types: dict[str, set[str]] = {}
        self._reaction_types: dict[str, set[str]] = {}
        self._monomers: dict[str, set[str]] = {}
        self._base_cofactor_pairs: dict[str, str] = dict()

        if compartments is not None:
            self.add_compartments(compartments=compartments)
        if compounds is not None:
            self.add_compounds(compounds=compounds)
        if cofactor_pairs is not None:
            for strong, weak in cofactor_pairs.items():
                self.add_cofactor_pair(strong, weak)
        if reactions is not None:
            self.add_reactions(reactions=reactions)
        if objective is not None:
            self.set_objective(objective=objective)

    def __repr__(self) -> str:
        s = f"Model: {self.name}\n"
        s += f"    compounds: {len(self.compounds)}\n"
        s += f"    reactions: {len(self.reactions)}\n"
        return s

    def __str__(self) -> str:
        s = f"Model: {self.name}\n"
        s += f"    compounds: {len(self.compounds)}\n"
        s += f"    reactions: {len(self.reactions)}\n"
        return s

    def __enter__(self) -> "Model":
        """Return and save a copy for context manager."""
        self._copy = self.copy()
        return self.copy()

    def __exit__(
        self,
        exception_type: Any,
        exception_value: Any,
        exception_traceback: Any,
    ) -> None:
        """Restore any changes made to the model."""
        self.__dict__ = self._copy.__dict__

    def __add__(self, other: object) -> "Model":
        if not isinstance(other, Model):
            return NotImplemented
        m1 = self.copy()
        m1.add_compartments(other.compartments)
        m1.add_compounds(other.compounds.values())
        m1.add_reactions(other.reactions.values())
        return m1

    def __iadd__(self, other: object) -> "Model":
        if not isinstance(other, Model):
            return NotImplemented
        self.add_compartments(other.compartments)
        self.add_compounds(other.compounds.values())
        self.add_reactions(other.reactions.values())
        return self

    def __sub__(self, other: object) -> "Model":
        if not isinstance(other, Model):
            return NotImplemented
        m = self.copy()
        m.remove_compartments([k for k in self.compartments.keys() if k in other.compartments])
        m.remove_compounds([k for k in self.compounds.keys() if k in other.compounds])
        m.remove_reactions([k for k in self.reactions.keys() if k in other.reactions])
        return m

    def __isub__(self, other: object) -> "Model":
        if not isinstance(other, Model):
            return NotImplemented
        self.remove_compartments([k for k in self.compartments.keys() if k in other.compartments])
        self.remove_compounds([k for k in self.compounds.keys() if k in other.compounds])
        self.remove_reactions([k for k in self.reactions.keys() if k in other.reactions])
        return self

    def __and__(self, other: object) -> "Model":
        if not isinstance(other, Model):
            return NotImplemented
        m = self.copy()
        m.add_compartments({k: v for k, v in self.compartments.items() if k in other.compartments})
        m.add_compounds([v for k, v in self.compounds.items() if k in other.compounds])
        m.add_reactions([v for k, v in self.reactions.items() if k in other.reactions])
        return m

    def __or__(self, other: object) -> "Model":
        if not isinstance(other, Model):
            return NotImplemented
        return self + other

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Model):
            return NotImplemented
        return all(getattr(self, i) == getattr(other, i) for i in self.__dict__)

    def copy(self) -> "Model":
        """Create a deepcopy of the reaction.

        While this is more costly than shallow copies, it takes away
        the hassle of always keeping track if a shallow copy is what
        you want at the moment. So it's mostly for me not getting
        confused ;)

        Returns
        -------
        self: moped.Model
        """
        return copy.deepcopy(self)

    def add_compartment(self, compartment_id: str, compartment_suffix: str) -> None:
        """Add a compartment to the model.

        Examples
        --------
        model.add_compartment(compartment_id='cytosol', compartment_suffix='c')
        """
        self.compartments[compartment_id] = compartment_suffix

    def add_compartments(self, compartments: dict[str, str]) -> None:
        """Add multiple compartments to the model.

        Examples
        --------
        model.add_compartments(compartments={'cytosol': 'c'})
        """
        for compartment_id, compartment_suffix in compartments.items():
            self.add_compartment(
                compartment_id=compartment_id,
                compartment_suffix=compartment_suffix,
            )

    def remove_compartment(self, compartment: str) -> None:
        del self.compartments[compartment]

    def remove_compartments(self, compartments: Iterable[str]) -> None:
        for i in compartments:
            self.remove_compartment(compartment=i)

    ##########################################################################
    # Utils
    ##########################################################################

    def _add_compartment_suffix(self, object_id: str, compartment_id: str) -> str:
        """Add a compartment suffix (e.g. _e for extracellular) to the id.

        Raises
        ------
        KeyError
            If compartment does not exist
        """
        suffix = self.compartments[compartment_id]
        if suffix != "":
            return object_id + f"_{suffix}"
        return object_id

    ##########################################################################
    # Universal functions
    ##########################################################################

    def create_submodel(self, reaction_ids: Iterable[str], name: str | None = None) -> "Model":
        """Create a subset of the model, containing the given reactions and their compounds."""
        reactions = [self.reactions[i] for i in sorted(reaction_ids)]
        compounds = set()
        for rec in reactions:
            for cpd_name in rec.stoichiometries:
                cpd = self.compounds[cpd_name].copy()
                cpd.in_reaction = set()
                compounds.add(cpd)
        if name is None:
            name = self.name + " submodel"

        submodel = Model(
            compounds=compounds,
            reactions=reactions,
            compartments=self.compartments.copy(),
            objective=self.objective.copy(),
            name=name,
            minimal_seed=self.minimal_seed.copy(),
            cofactor_pairs=self.cofactor_pairs.copy(),
        )

        # submodel.cofactor_pairs = self.cofactor_pairs.copy()
        # This is wrong, submodel must not necessarily have the same cofactor pairs

        # Collect base cofactor pairs
        base_cofactor_pairs = {}
        for strong, weak in self.cofactor_pairs.items():
            strong_base_cpd = self.compounds[strong].base_id
            weak_base_cpd = self.compounds[weak].base_id
            base_cofactor_pairs[strong_base_cpd] = weak_base_cpd

        # Add cofactor pairs to submodel, that actually exist in there
        for strong, weak in base_cofactor_pairs.items():
            submodel.add_cofactor_pair(
                strong_cofactor_base_id=strong,
                weak_cofactor_base_id=weak,
            )
        return submodel

    def add_cofactor_pair(self, strong_cofactor_base_id: str, weak_cofactor_base_id: str) -> None:
        """Add a cofactor pair.

        This automatically adds all compartment variants of the given base ids.

        Examples
        --------
        >>> model.add_cofactor_pair("ATP", "ADP")
        """
        try:
            self._base_cofactor_pairs[strong_cofactor_base_id] = weak_cofactor_base_id
            for strong_cpd_id in self.base_compounds[strong_cofactor_base_id]:
                for weak_cpd_id in self.base_compounds[weak_cofactor_base_id]:
                    if self.compounds[strong_cpd_id].compartment == self.compounds[weak_cpd_id].compartment:
                        self.cofactor_pairs[strong_cpd_id] = weak_cpd_id
        except KeyError:
            pass

    def get_weak_cofactors(self) -> list[str]:
        """Get ids of weak cofactors."""
        return list(set([i for i in self.cofactor_pairs.values()]))

    def get_weak_cofactor_duplications(self) -> list[str]:
        """Get ids of weak cofactors including the __cof__ tag.

        This function is useful for structural analyses, in which these
        tagged cofactors are used.
        """
        return list(set([i + "__cof__" for i in self.cofactor_pairs.values()]))

    def get_strong_cofactors(self) -> list[str]:
        """Get ids of strong cofactors."""
        return list(set([i for i in self.cofactor_pairs.keys()]))

    def get_strong_cofactor_duplications(self) -> list[str]:
        """Get ids of strong cofactors including the __cof__ tag.

        This function is useful for structural analyses, in which these
        tagged cofactors are used."""
        return list(set([i + "__cof__" for i in self.cofactor_pairs.keys()]))

    def update_from_reference(
        self, reference_model: "Model", verbose: bool = False
    ) -> tuple[list[str], set[str]]:
        """Update a model from a reference Model.

        Returns
        -------
        unmapped_reactions
            Reactions that could not be found in the reference database
        unmapped_compounds
            Compounds that could not be found in the reference database
        """
        mapped_compounds = set(self.compounds).intersection(reference_model.compounds)
        unmapped_compounds = set(self.compounds).difference(reference_model.compounds)

        old_base_reactions = set(self.base_reactions)
        old_variant_reactions = set(self.variant_reactions)

        new_base_reactions = set(reference_model.base_reactions)
        new_variant_reactions = set(reference_model.variant_reactions)

        unmapped_base_reactions = [
            j for i in old_base_reactions.difference(new_base_reactions) for j in self.base_reactions[i]
        ]
        unmapped_variant_reactions = [
            j
            for i in old_variant_reactions.difference(new_variant_reactions)
            for j in self.variant_reactions[i]
        ]
        unmapped_reactions = unmapped_base_reactions + unmapped_variant_reactions

        # Update all existing compounds
        for compound_id in mapped_compounds:
            self.add_compound_from_reference(reference_model=reference_model, compound_id=compound_id)

        # Update all existing base reactions
        for base_reaction_id in old_base_reactions.intersection(
            new_base_reactions
        ) | old_variant_reactions.intersection(new_variant_reactions):
            self.add_reaction_from_reference(
                reference_model=reference_model,
                reaction_id=base_reaction_id,
                update_compounds=True,
            )

        # Updating compounds can change the balance status
        # of the local reactions, thus those that cannot be mapped
        # need to be checked again
        for reaction_id in unmapped_reactions:
            if not self.check_mass_balance(reaction_id=reaction_id):
                self.remove_reaction(reaction_id=reaction_id)
                continue
            if not self.check_charge_balance(reaction_id=reaction_id):
                self.remove_reaction(reaction_id=reaction_id)
        if verbose:
            print(
                f"Could not map {len(unmapped_base_reactions) + len(unmapped_variant_reactions)} reactions "
                + f"and {len(unmapped_compounds)} compounds"
            )
        return unmapped_reactions, unmapped_compounds

    ##########################################################################
    # Compound functions
    ##########################################################################

    def add_compound(self, compound: Compound) -> None:
        """Add a compound to the model. Overwrites existing compounds."""
        if isinstance(compound, Compound):
            if not bool(compound.id):
                compound.id = self._add_compartment_suffix(
                    object_id=compound.base_id,
                    compartment_id=cast(str, compound.compartment),
                )
            cpd_id = cast(str, compound.id)
            self.compounds[cpd_id] = compound.copy()
            self.base_compounds.setdefault(compound.base_id, set()).add(cpd_id)
            for compound_type in compound.types:
                self._compound_types.setdefault(compound_type, set()).add(cpd_id)
        else:
            raise TypeError("Compound has to be of type moped.model.Compound")

    def add_compounds(self, compounds: Iterable[Compound]) -> None:
        """Add multiple compounds to the model. Overwrites existing compounds."""
        for compound in compounds:
            self.add_compound(compound=compound)

    def add_compound_from_reference(self, reference_model: "Model", compound_id: str) -> None:
        """Overwrite local data from reference database or adds new one if it does not exist already."""
        new_cpd = reference_model.compounds[compound_id].copy()
        try:
            old_cpd = self.compounds.pop(compound_id)
            new_cpd.in_reaction = old_cpd.in_reaction
        except KeyError:
            new_cpd.in_reaction = set()
        self.compounds[compound_id] = new_cpd

    def _create_compartment_variant(self, old_compound: Compound, compartment_id: str) -> Compound:
        """Create a variant of the compound in another compartment.

        This empties the in_reaction set, as the compound is only known
        to be part of the reactions in the previous compartment and
        we cannot know whether those reactions are also available
        in the new compartment.
        """
        new_compound = old_compound.copy()
        new_compound.id = self._add_compartment_suffix(
            object_id=old_compound.base_id, compartment_id=compartment_id
        )
        new_compound.compartment = compartment_id
        new_compound.in_reaction = set()
        self.add_compound(compound=new_compound)
        return new_compound

    def add_compartment_compound_variant(self, compound_id: str, compartment_id: str) -> Compound:
        """Add a copy of the compound in the respective compartment."""
        try:
            old_compound = self.compounds[compound_id]
        except KeyError:
            try:
                compound_variants = self.base_compounds[compound_id]
                old_compound = self.compounds[next(iter(compound_variants))]
            except KeyError:
                raise KeyError(f"Compound {compound_id} has to be in the model to create an external variant")
        new_compound_id = self._add_compartment_suffix(
            object_id=old_compound.base_id, compartment_id=compartment_id
        )
        try:
            new_compound = self.compounds[new_compound_id]
        except KeyError:
            new_compound = self._create_compartment_variant(
                old_compound=old_compound, compartment_id=compartment_id
            )
        return new_compound

    def set_compound_property(self, compound_id: str, property_dict: dict[str, Any]) -> None:
        """Set one or multiple properties of a compound."""
        for k, v in property_dict.items():
            cpd = self.compounds[compound_id]
            slots = dataclasses.asdict(cpd).keys()
            if k not in slots:
                raise KeyError(f"Compound does not have key '{k}', can only be one of {slots}")
            setattr(cpd, k, v)

    def remove_compound(self, compound_id: str) -> None:
        """Remove a compound from the model."""
        compound = self.compounds.pop(compound_id)

        # Also remove from base compounds
        self.base_compounds[compound.base_id].remove(compound_id)
        if not bool(self.base_compounds[compound.base_id]):
            del self.base_compounds[compound.base_id]

        # Also remove from compound types
        for compound_type in compound.types:
            self._compound_types[compound_type].remove(compound_id)
            if not bool(self._compound_types[compound_type]):
                del self._compound_types[compound_type]

        if compound_id in self.cofactor_pairs:
            del self.cofactor_pairs[compound_id]
        elif compound_id in self.get_weak_cofactors():
            weak_cofactors = self.get_weak_cofactors()
            strong_cofactors = self.get_strong_cofactors()
            weak_to_strong = dict(zip(weak_cofactors, strong_cofactors))
            del self.cofactor_pairs[weak_to_strong[compound_id]]

    def remove_compounds(self, compound_ids: Iterable[str]) -> None:
        """Remove multiple compounds from the model."""
        for compound_id in compound_ids:
            self.remove_compound(compound_id=compound_id)

    def remove_unused_compounds(self) -> None:
        """Remove compounds from the model that are in no reaction."""
        all_compounds = set(self.compounds)
        used_compounds = set()
        for reaction in self.reactions.values():
            used_compounds.update(set(reaction.stoichiometries))
        unused = all_compounds.difference(used_compounds)
        self.remove_compounds(compound_ids=unused)

    def get_compound_base_id(self, compound_id: str) -> str:
        """Get the database links of a given compound."""
        return self.compounds[compound_id].base_id

    def get_compound_compartment_variants(self, compound_base_id: str) -> set[str]:
        """Get compound ids for all respective compartments the compound is in.

        The compound_base_id for ATP_c for example would be ATP.
        """
        return self.base_compounds[compound_base_id]

    def get_compound_compartment(self, compound_id: str) -> str | None:
        """Get compartment of a given compound compound."""
        return self.compounds[compound_id].compartment

    def get_compound_formula(self, compound_id: str) -> dict[str, float] | None:
        """Get the charge of a given compound."""
        return self.compounds[compound_id].formula

    def get_compound_charge(self, compound_id: str) -> float | None:
        """Get the charge of a given compound."""
        return self.compounds[compound_id].charge

    def get_compound_gibbs0(self, compound_id: str) -> float | None:
        """Get the gibbs energy (free enthalpy) of the given compound."""
        return self.compounds[compound_id].gibbs0

    def get_reactions_of_compound(self, compound_id: str) -> set[str]:
        """Get all reactions of a compound."""
        return self.compounds[compound_id].in_reaction

    def get_compound_database_links(self, compound_id: str) -> dict[str, set[str]]:
        """Get the database links of a given compound."""
        return self.compounds[compound_id].database_links

    def get_base_compound_ids(self) -> set[str]:
        """Get base IDs of all compounds."""
        return set(i.base_id for i in self.compounds.values())

    def get_compound_type_ids(self) -> set[str]:
        """Get all available compound types."""
        return set(self._compound_types)

    def get_model_compartment_ids(self) -> set[str]:
        """Get all ids for compartments used in the model."""
        return set(self.compartments)

    def get_compounds_of_compartment(self, compartment_id: str) -> list[str]:
        """Get all compounds from the respective compartment.

        To look up the available compartments, see model.get_model_compartment_ids

        See Also
        --------
        model.get_model_compartment_ids
            To get all available compartments
        """
        if compartment_id not in self.get_model_compartment_ids():
            raise KeyError(
                f"Unknown compartment {compartment_id}, did you mean any of {self.get_model_compartment_ids()}?"
            )
        return [k for k, v in self.compounds.items() if v.compartment == compartment_id]

    def get_compounds_of_type(self, compound_type: str) -> set[str]:
        """Get all compound ids of a given compound_type."""
        return self._compound_types[compound_type]

    ##########################################################################
    # Reaction functions
    ##########################################################################

    def add_reaction(self, reaction: Reaction) -> None:
        """Add a reaction to the model."""
        if not isinstance(reaction, Reaction):
            raise TypeError("Reaction has to be of type moped.model.Reaction")
        reaction_id = reaction.id
        if reaction._var is not None:
            self.variant_reactions.setdefault(cast(str, reaction.base_id), set()).add(reaction_id)
        else:
            self.base_reactions.setdefault(cast(str, reaction.base_id), set()).add(reaction_id)
        self.reactions[reaction_id] = reaction.copy()
        for compound in reaction.stoichiometries:
            self.compounds[compound].in_reaction.add(reaction_id)
        for type_ in reaction.types:
            self._reaction_types.setdefault(type_, set()).add(reaction.id)
        for pathway in reaction.pathways:
            self.add_reaction_to_pathway(pathway_id=pathway, reaction_id=reaction_id)
        for monomer in reaction.sequences:
            self._monomers.setdefault(monomer, set()).add(reaction.id)

    def add_reactions(self, reactions: Iterable[Reaction]) -> None:
        """Add multiple reactions to the model."""
        for reaction in reactions:
            self.add_reaction(reaction=reaction)

    def add_reaction_from_reference(
        self,
        reference_model: "Model",
        reaction_id: str,
        update_compounds: bool = True,
    ) -> None:
        """Add a reaction from a reference model.

        Always adds reversibiliy and cofactor duplicates as well. In this case all
        existing reaction variants are kept if they are not overwritten.
        Adds all variants of a reaction if the base_id of a variant reaction is given.
        In this case all other existing reaction variants are removed.
        """
        try:
            reaction = self.reactions[reaction_id]
            base_id = cast(str, reaction.base_id)
        except KeyError:
            base_id = reaction_id
        if base_id in reference_model.variant_reactions:
            try:
                for reaction_id in tuple(self.variant_reactions[base_id]):
                    self.remove_reaction(reaction_id)
            except KeyError:
                pass
            new_reactions = [
                reference_model.reactions[reaction_id]
                for reaction_id in reference_model.variant_reactions[base_id]
            ]
            if update_compounds:
                new_compound_ids = {j for i in new_reactions for j in i.stoichiometries}
                for compound_id in new_compound_ids:
                    self.add_compound_from_reference(reference_model=reference_model, compound_id=compound_id)
            for reaction in new_reactions:
                self.add_reaction(reaction=reaction)
        elif base_id in reference_model.base_reactions:
            try:
                for reaction_id in tuple(self.base_reactions[base_id]):
                    self.remove_reaction(reaction_id=reaction_id)
            except KeyError:
                pass
            new_reactions = [
                reference_model.reactions[reaction_id]
                for reaction_id in reference_model.base_reactions[base_id]
            ]
            if update_compounds:
                new_compound_ids = {j for i in new_reactions for j in i.stoichiometries}
                for compound_id in new_compound_ids:
                    self.add_compound_from_reference(reference_model=reference_model, compound_id=compound_id)
            for reaction in new_reactions:
                self.add_reaction(reaction=reaction)
        elif base_id in reference_model.reactions:
            self.add_reaction_from_reference(
                reference_model=reference_model,
                reaction_id=cast(str, reference_model.reactions[reaction_id].base_id),
            )
        else:
            raise KeyError(f"Could not find {reaction_id} in the reference_model")

    def add_reactions_from_reference(
        self,
        reference_model: "Model",
        reaction_ids: Iterable[str],
        update_compounds: bool = True,
    ) -> None:
        """Add reactions from a reference model, overwriting existing reactions."""
        for reaction_id in reaction_ids:
            self.add_reaction_from_reference(
                reference_model=reference_model,
                reaction_id=reaction_id,
                update_compounds=update_compounds,
            )

    def set_reaction_property(self, reaction_id: str, property_dict: dict[str, Any]) -> None:
        """Set one or multiple properties of a reaction."""
        for k, v in property_dict.items():
            reaction = self.reactions[reaction_id]
            if k in reaction.__dict__:
                setattr(self.reactions[reaction_id], k, v)
            else:
                raise KeyError(f"Reaction does not have key '{k}', can only be one of {Reaction.__dict__}")

    def remove_reaction(self, reaction_id: str, remove_empty_references: bool = True) -> None:
        """Remove a reaction from the model."""
        reaction = self.reactions[reaction_id]
        base_id = cast(str, reaction.base_id)
        if reaction._var is not None:
            self.variant_reactions[base_id].remove(reaction_id)
            if remove_empty_references:
                if not bool(self.variant_reactions[base_id]):
                    del self.variant_reactions[base_id]
        else:
            self.base_reactions[base_id].remove(reaction_id)
            if remove_empty_references:
                if not bool(self.base_reactions[base_id]):
                    del self.base_reactions[base_id]
        for pathway in tuple(reaction.pathways):
            self.remove_reaction_from_pathway(pathway_id=pathway, reaction_id=reaction_id)
        for compound in reaction.stoichiometries:
            self.compounds[compound].in_reaction.remove(reaction_id)
            if remove_empty_references:
                if not bool(self.compounds[compound].in_reaction):
                    del self.compounds[compound]
        for monomer in reaction.sequences:
            self._monomers[monomer].remove(reaction_id)
            if not bool(self._monomers[monomer]):
                del self._monomers[monomer]
        for type_ in reaction.types:
            self._reaction_types[type_].remove(reaction_id)
            if remove_empty_references:
                if not bool(self._reaction_types[type_]):
                    del self._reaction_types[type_]
        del self.reactions[reaction_id]

    def remove_reactions(self, reaction_ids: Iterable[str]) -> None:
        """Remove multiple reactions from the model."""
        for reaction_id in reaction_ids:
            self.remove_reaction(reaction_id=reaction_id)

    def get_reaction_base_id(self, reaction_id: str) -> str | None:
        """Get the base id of a given reaction."""
        return self.reactions[reaction_id].base_id

    def get_reaction_compartment_variants(self, reaction_base_id: str) -> set[str]:
        """Get the ids of the reaction in all compartments it takes place in."""
        return self.base_reactions[reaction_base_id]

    def get_reaction_compartment(self, reaction_id: str) -> str | tuple[str, ...] | None:
        """Get the compartment of a given reaction."""
        return self.reactions[reaction_id].compartment

    def get_reaction_gibbs0(self, reaction_id: str) -> float | None:
        """Get the database links of a given reaction."""
        return self.reactions[reaction_id].gibbs0

    def get_reaction_bounds(self, reaction_id: str) -> tuple[float, float] | None:
        """Get the bounds of a given reaction."""
        return self.reactions[reaction_id].bounds

    def get_reaction_reversibility(self, reaction_id: str) -> bool | None:
        """Get whether a reaction is reversible."""
        return self.reactions[reaction_id].reversible

    def get_reaction_pathways(self, reaction_id: str) -> set[str]:
        """Get the pathways of a given reaction."""
        return self.reactions[reaction_id].pathways

    def get_reaction_sequences(self, reaction_id: str) -> dict[str, str]:
        """Get the protein sequences of a given reaction."""
        return self.reactions[reaction_id].sequences

    def get_reaction_types(self, reaction_id: str) -> list[str]:
        """Get the types of a given reaction."""
        return self.reactions[reaction_id].types

    def get_reaction_database_links(self, reaction_id: str) -> dict[str, set[str]]:
        """Get the database links of a given reaction."""
        return self.reactions[reaction_id].database_links

    def get_base_reaction_ids(self) -> set[str]:
        """Get base IDs of all reactions."""
        return set(self.base_reactions)

    def get_reaction_type_ids(self) -> set[str]:
        """Get all available reaction types."""
        return set(self._reaction_types)

    def get_reactions_of_type(self, reaction_type: str) -> set[str]:
        """Get all reaction ids of a given type."""
        return self._reaction_types[reaction_type]

    def get_reaction_variants(self, base_reaction_id: str) -> set[str]:
        """Get all reaction variants."""
        return self.variant_reactions[base_reaction_id]

    def get_reversible_reactions(self) -> list[str]:
        """Get all reactions marked as reversible."""
        return [k for k, v in self.reactions.items() if v.reversible]

    def get_irreversible_reactions(self) -> list[str]:
        """Get all reactions marked as irreversible."""
        return [k for k, v in self.reactions.items() if not v.reversible]

    def get_transmembrane_reactions(self) -> list[str]:
        """Get reaction ids for reactions with compounds in two compartments."""
        return [k for k, v in self.reactions.items() if v.transmembrane]

    def get_reactions_of_compartment(
        self, compartment_id: str, include_transporters: bool = True
    ) -> set[str]:
        """Reaction reaction ids for reactions that occur in a given compartment."""
        reaction_ids = {
            reaction_id
            for compound_id in self.get_compounds_of_compartment(compartment_id=compartment_id)
            for reaction_id in self.compounds[compound_id].in_reaction
        }
        if include_transporters:
            return reaction_ids
        return reaction_ids.difference(self.get_transmembrane_reactions())

    def get_transport_reactions(self, compartment_id: str) -> set[str]:
        """Get reactions that transport something in/out of a given compartment."""
        compartment_reactions = self.get_reactions_of_compartment(
            compartment_id=compartment_id, include_transporters=True
        )
        transmembrane_reactions = self.get_transmembrane_reactions()
        return set(transmembrane_reactions).intersection(compartment_reactions)

    ##########################################################################
    # Pathway functions
    ##########################################################################

    def add_pathway(self, pathway_id: str, pathway_reactions: Iterable[str]) -> None:
        """Add a pathway to the model."""
        for reaction_id in pathway_reactions:
            self.reactions[reaction_id].pathways.add(pathway_id)
        self.pathways.setdefault(pathway_id, set()).update(pathway_reactions)

    def add_reaction_to_pathway(self, pathway_id: str, reaction_id: str) -> None:
        """Add a reaction to a pathway."""
        self.reactions[reaction_id].pathways.add(pathway_id)
        self.pathways.setdefault(pathway_id, set()).add(reaction_id)

    def remove_pathway(self, pathway_id: str) -> None:
        """Remove a pathway from the model."""
        reactions = self.pathways.pop(pathway_id)
        for reaction in reactions:
            self.reactions[reaction].pathways.remove(pathway_id)

    def remove_reaction_from_pathway(self, pathway_id: str, reaction_id: str) -> None:
        """Remove a reaction from a pathway."""
        self.pathways[pathway_id].remove(reaction_id)
        self.reactions[reaction_id].pathways.remove(pathway_id)
        # Remove pathway completely if it is empty
        if self.pathways[pathway_id] == set():
            self.remove_pathway(pathway_id=pathway_id)

    def get_reactions_of_pathway(self, pathway_id: str) -> set[str]:
        """Get all reactions that are part of a pathway."""
        return self.pathways[pathway_id]

    def get_pathway_ids(self) -> list[str]:
        """Get all pathway ids."""
        return list(self.pathways.keys())

    ##########################################################################
    # Monomer functions
    ##########################################################################

    def get_monomer_sequences(self, reaction_ids: Iterable[str]) -> set[str]:
        """Get monomer sequences from the given reaction_ids."""
        sequences = set()
        for reaction_id in reaction_ids:
            reaction = self.reactions[reaction_id]
            for name, sequence in reaction.sequences.items():
                sequences.add(f">gnl|META|{name}\n{sequence}")
        return sequences

    def get_all_monomer_sequences(self) -> set[str]:
        """Get monomer sequences of all reactions."""
        sequences = set()
        for reaction in self.reactions.values():
            for name, sequence in reaction.sequences.items():
                sequences.add(f">gnl|META|{name}\n{sequence}")
        return sequences

    ##########################################################################
    # Medium, biomass and objective functions
    ##########################################################################

    def add_transport_reaction(
        self,
        compound_id: str,
        compartment_id: str,
        bounds: tuple[float, float] = (-1000, 1000),
    ) -> None:
        """Add a transport reaction into another compartment."""
        into_cpd = self.add_compartment_compound_variant(
            compound_id=compound_id, compartment_id=compartment_id
        )
        into_suffix = self._add_compartment_suffix(object_id="", compartment_id=compartment_id)
        self.add_reaction(
            Reaction(
                id=f"TR_{compound_id}{into_suffix}",
                base_id=f"TR_{compound_id}",
                stoichiometries={compound_id: -1, cast(str, into_cpd.id): 1},
                bounds=bounds,
                transmembrane=True,
            )
        )

    def add_influx(self, compound_id: str, extracellular_compartment_id: str) -> None:
        """Add an influx of a compound to the model."""
        # Add influx
        ex_met = self.add_compartment_compound_variant(
            compound_id=compound_id, compartment_id=extracellular_compartment_id
        )
        self.add_reaction(
            Reaction(
                id=f"EX_{ex_met.base_id}_e",
                base_id=f"EX_{ex_met.base_id}",
                stoichiometries={cast(str, ex_met.id): -1},
                bounds=(-1000, 0),
            )
        )

    def remove_influx(self, compound_id: str) -> None:
        """Remove the influx of a given compound."""
        try:
            compound = self.compounds[compound_id]
            base_compound_id = compound.base_id
        except KeyError:
            if compound_id not in self.base_compounds:
                raise KeyError(f"Compound {compound_id} neither found in compounds nor base compounds")
            base_compound_id = compound_id
        self.remove_reaction(reaction_id=f"EX_{base_compound_id}_e")

    def add_efflux(self, compound_id: str, extracellular_compartment_id: str) -> None:
        """Add an efflux of a compound to the model."""
        # Add efflux
        ex_met = self.add_compartment_compound_variant(
            compound_id=compound_id, compartment_id=extracellular_compartment_id
        )
        self.add_reaction(
            Reaction(
                id=f"EX_{ex_met.base_id}_e",
                base_id=f"EX_{ex_met.base_id}",
                stoichiometries={cast(str, ex_met.id): -1},
                bounds=(0, 1000),
            )
        )

    def remove_efflux(self, compound_id: str) -> None:
        """Remove the efflux of a given compound."""
        try:
            compound = self.compounds[compound_id]
            base_compound_id = compound.base_id
        except KeyError:
            if compound_id not in self.base_compounds:
                raise KeyError(f"Compound {compound_id} neither found in compounds nor base compounds")
            base_compound_id = compound_id
        self.remove_reaction(reaction_id=f"EX_{base_compound_id}_e")

    def add_medium_component(self, compound_id: str, extracellular_compartment_id: str) -> None:
        """Add a compound as a medium component."""
        # Add medium influx/efflux
        ex_met = self.add_compartment_compound_variant(
            compound_id=compound_id, compartment_id=extracellular_compartment_id
        )
        self.add_reaction(
            Reaction(
                id=f"EX_{ex_met.base_id}_e",
                base_id=f"EX_{ex_met.base_id}",
                stoichiometries={cast(str, ex_met.id): -1},
                bounds=(-1000, 1000),
            )
        )

    def remove_medium_component(self, compound_id: str) -> None:
        """Remove influx and outflux of a given compound."""
        try:
            compound = self.compounds[compound_id]
            base_compound_id = compound.base_id
        except KeyError:
            if compound_id not in self.base_compounds:
                raise KeyError(f"Compound {compound_id} neither found in compounds nor base compounds")
            base_compound_id = compound_id
        self.remove_reaction(reaction_id=f"EX_{base_compound_id}_e")

    def set_objective(self, objective: dict[str, float]) -> None:
        """Set the objective function(s)."""
        for reaction_id in objective:
            if reaction_id not in self.reactions:
                raise KeyError(f"Objective reaction {reaction_id} is not in the model")
        self.objective = dict(objective)

    ##########################################################################
    # Quality control interface
    ##########################################################################

    def check_charge_balance(self, reaction_id: str, verbose: bool = False) -> bool:
        """Check the charge balance of a reaction."""
        substrate_charge = 0.0
        product_charge = 0.0
        for k, v in self.reactions[reaction_id].stoichiometries.items():
            charge = self.compounds[k].charge
            if charge is None:
                return False
            if v < 0:
                substrate_charge -= charge * v
            else:
                product_charge += charge * v
        if verbose:
            print(f"Substrate charge: {substrate_charge}")
            print(f"Product charge: {product_charge}")
        if substrate_charge - product_charge == 0:
            return True
        return False

    def check_mass_balance(self, reaction_id: str, verbose: bool = False) -> bool:
        """Check the mass balance of a reaction."""
        lhs_atoms: DefaultDict[str, float] = defaultdict(int)
        rhs_atoms: DefaultDict[str, float] = defaultdict(int)
        for k, v in self.reactions[reaction_id].stoichiometries.items():
            formula = self.compounds[k].formula
            if not bool(formula):
                return False
            if v < 0:
                for atom, stoich in formula.items():
                    lhs_atoms[atom] -= stoich * v
            else:
                for atom, stoich in formula.items():
                    rhs_atoms[atom] += stoich * v
        if verbose:
            print(dict(lhs_atoms))
            print(dict(rhs_atoms))
        for k in set((*lhs_atoms, *rhs_atoms)):
            diff = lhs_atoms[k] - rhs_atoms[k]
            if diff != 0:
                return False
        return True

    ###########################################################################
    # Stoichiometric functions
    ###########################################################################

    def get_stoichiometric_matrix(self) -> np.ndarray:
        """Return the stoichiometric matrix."""
        cpd_mapper = dict(zip(self.compounds, range(len(self.compounds))))
        N = np.zeros((len(self.compounds), len(self.reactions)))
        for i, rxn in enumerate(self.reactions.values()):
            for cpd, val in rxn.stoichiometries.items():
                N[cpd_mapper[cpd], i] = val
        return N

    def get_stoichiometric_df(self) -> pd.DataFrame:
        """Return the stoichiometric matrix as an annotated pandas dataframe."""
        return pd.DataFrame(
            self.get_stoichiometric_matrix(),
            index=cast(list, self.compounds),  # actually keys(), but doesn't matter
            columns=cast(list, self.reactions),  # actually keys(), but doesn't matter
        )

    ##########################################################################
    # Structural functions
    ##########################################################################

    def add_minimal_seed(self, compound_ids: Iterable[str]) -> None:
        """Add compounds that make up a minimal seed for the given organism."""
        for compound in compound_ids:
            self.minimal_seed.add(compound)

    def get_minimal_seed(self, carbon_source_id: str) -> set[str]:
        """Get a minimal seed for most organisms."""
        if not bool(self.minimal_seed):
            raise ValueError(
                "No minimal seed defined for this database. You can define one with Model.add_minimal_seed"
            )
        seed = self.minimal_seed.copy()
        seed.add(carbon_source_id)
        return seed
