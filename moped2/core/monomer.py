from __future__ import annotations

from dataclasses import dataclass, field


@dataclass
class Monomer:
    id: str
    gene: str
    database_links: dict[str, set[str]] = field(default_factory=dict)
