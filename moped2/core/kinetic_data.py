from __future__ import annotations

from dataclasses import dataclass, field


@dataclass
class KineticData:
    km: dict[str, float] = field(default_factory=dict)
    kcat: dict[str, float] = field(default_factory=dict)
