from __future__ import annotations

__all__ = ["pipeline", "parse", "repair"]

from pathlib import Path

from ..data import Cyc, ParseCompound
from . import parse, repair


def pipeline(
    pgdb_path: Path,
    type_map: dict[str, str],
    manual_additions: dict[str, ParseCompound],
    compartment_map: dict[str, str],
    compartment_suffixes: dict[str, str],
) -> Cyc:
    parse_result = parse.parse(pgdb_path)
    return repair.repair(parse_result, type_map, manual_additions, compartment_map, compartment_suffixes)
