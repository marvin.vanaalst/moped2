from __future__ import annotations

from .....core.reaction import KineticData
from ...data import ParseResult


def fix_kinetic_data(result: ParseResult) -> ParseResult:
    for rxn in result.reactions.values():
        rxn.kinetic_data = {
            k: KineticData(km=v.get("km", {}), kcat=v.get("kcat", {})) for k, v in rxn.enzrxns.items()
        }
    return result
