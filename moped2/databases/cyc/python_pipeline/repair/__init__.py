from __future__ import annotations

from ...data import Cyc, ParseCompound, ParseResult

from .add_important_cpds import fix_add_important_compounds
from .annotate_monomers import fix_annotate_monomers
from .create_compartment_variants import fix_create_compartment_variants
from .create_reaction_variants import fix_create_reaction_variants
from .filter_garbage import fix_filter_garbage_reactions
from .kinetic_data import fix_kinetic_data
from .set_reaction_stoichiometry import fix_set_reaction_stoichiometry
from .unify_reaction_direction import fix_unify_reaction_direction
from .rename_types import fix_rename_types


def repair(
    result: ParseResult,
    type_map: dict[str, str],
    manual_additions: dict[str, ParseCompound],
    compartment_map: dict[str, str],
    compartment_suffixes: dict[str, str],
) -> Cyc:
    result = fix_rename_types(result, type_map)
    result = fix_add_important_compounds(result, manual_additions)
    result = fix_unify_reaction_direction(result)
    result = fix_kinetic_data(result)
    result = fix_annotate_monomers(result)

    # Larger changes
    result = fix_create_reaction_variants(result)
    result = fix_filter_garbage_reactions(result)
    result = fix_create_compartment_variants(
        result,
        compartment_map,
        compartment_suffixes,
    )
    result = fix_set_reaction_stoichiometry(result)
    return Cyc(result.compounds, result.reactions)
