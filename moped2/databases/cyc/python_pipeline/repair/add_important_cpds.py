from __future__ import annotations

from ...data import ParseCompound, ParseResult


def fix_add_important_compounds(
    parse_results: ParseResult,
    manual_additions: dict[str, ParseCompound],
) -> ParseResult:
    parse_results.compounds.update(manual_additions)
    return parse_results
