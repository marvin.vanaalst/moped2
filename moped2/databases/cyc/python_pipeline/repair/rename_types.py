from __future__ import annotations

from ...data import ParseResult


def fix_rename_types(res: ParseResult, type_map: dict[str, str]) -> ParseResult:
    for compound in res.compounds.values():
        compound.types = [type_map.get(k, k) for k in compound.types]

    for reaction in res.reactions.values():
        reaction.types = [type_map.get(k, k) for k in reaction.types]
        reaction.substrates = {type_map.get(k, k): v for k, v in reaction.substrates.items()}
        reaction.products = {type_map.get(k, k): v for k, v in reaction.products.items()}
        reaction.substrate_compartments = {
            type_map.get(k, k): v for k, v in reaction.substrate_compartments.items()
        }
        reaction.product_compartments = {
            type_map.get(k, k): v for k, v in reaction.product_compartments.items()
        }
    return res
