from __future__ import annotations

from ...data import ParseResult


def fix_annotate_monomers(result: ParseResult) -> ParseResult:
    monomers = result.genes
    for rxn in result.reactions.values():
        rxn.monomers_annotated = {
            k: {monomer: monomers[monomer] for monomer in v if monomer in monomers}
            for k, v in rxn.monomers.items()
        }
        # del rxn.monomers
    return result
