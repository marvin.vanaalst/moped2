from __future__ import annotations

from ...data import ParseResult
from ...utils import _reaction_is_bad


def fix_filter_garbage_reactions(result: ParseResult) -> ParseResult:
    compounds = result.compounds
    new_reactions = {}
    for rxn_id, reaction in result.reactions.items():
        if _reaction_is_bad(reaction, compounds):
            continue
        new_reactions[rxn_id] = reaction
    result.reactions = new_reactions
    return result
