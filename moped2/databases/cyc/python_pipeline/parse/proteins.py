from __future__ import annotations

import logging
from typing import Any, Callable, Iterable

from .shared import MALFORMED_LINE_STARTS

logger = logging.getLogger(__name__)


def _add_component(complexes: dict[str, set[str]], complex_id: str, component: str) -> None:
    complexes[complex_id].add(component)


def parse_proteins(file: Iterable[str]) -> tuple[set[str], dict[str, set[str]]]:
    actions: dict[str, Callable[[Any, Any, Any], None]] = {
        "COMPONENTS": _add_component,
    }
    id_ = ""
    proteins: dict[str, set[str]] = {}
    monomers: set[str] = set()
    complexes: dict[str, set[str]] = dict()
    for line in file:
        if any(line.startswith(i) for i in MALFORMED_LINE_STARTS):
            continue
        try:
            identifier, content = line.rstrip().split(" - ", maxsplit=1)
        except ValueError:
            logger.info(f"Malformed line in proteins.dat: {line}")
            continue

        if identifier == "UNIQUE-ID":
            id_ = content
            proteins[id_] = set()
        elif not identifier.startswith("^"):
            if (action := actions.get(identifier, None)) is not None:
                action(proteins, id_, content)

    for k, v in proteins.items():
        if bool(v):
            complexes[k] = v
        else:
            monomers.add(k)
    return monomers, complexes
