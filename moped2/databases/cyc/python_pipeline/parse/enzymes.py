from __future__ import annotations

import logging
from typing import Any, Callable, Iterable

from ...data import ParseEnzyme
from .shared import MALFORMED_LINE_STARTS


logger = logging.getLogger(__name__)


def _set_enzyme(enzrxns: dict[str, ParseEnzyme], id_: str, enzyme: str) -> None:
    enzrxns[id_].enzyme = enzyme


def _add_kcat(enzrxns: dict[str, ParseEnzyme], id_: str, substrate: str, kcat: str) -> None:
    try:
        enzrxns[id_].kcat.setdefault(substrate, float(kcat))
    except ValueError:  # conversion failed
        pass


def _add_km(enzrxns: dict[str, ParseEnzyme], id_: str, substrate: str, km: str) -> None:
    try:
        enzrxns[id_].km.setdefault(substrate, float(km))
    except ValueError:  # conversion failed
        pass


def _add_vmax(enzrxns: dict[str, ParseEnzyme], id_: str, substrate: str, vmax: str) -> None:
    try:
        enzrxns[id_].vmax.setdefault(substrate, float(vmax))
    except ValueError:  # conversion failed
        pass


def parse_enzymes(file: Iterable[str]) -> dict[str, ParseEnzyme]:
    actions: dict[str, Callable[[Any, Any, Any], None]] = {
        "ENZYME": _set_enzyme,
    }
    sub_actions: dict[str, dict[str, Callable[[Any, Any, Any, Any], None]]] = {
        "^SUBSTRATE": {"KM": _add_km, "VMAX": _add_vmax, "KCAT": _add_kcat},
    }

    id_ = ""
    enzrxns = {}
    last_identifier = ""
    last_content = ""
    for line in file:
        if any(line.startswith(i) for i in MALFORMED_LINE_STARTS):
            continue
        try:
            identifier, content = line.rstrip().split(" - ", maxsplit=1)
        except ValueError:
            logger.info(f"Malformed line in enzymes.dat {line}")
            continue

        if identifier == "UNIQUE-ID":
            id_ = content
            enzrxns[id_] = ParseEnzyme(id=id_)
        elif identifier.startswith("^"):
            if (subaction := sub_actions.get(identifier, {}).get(last_identifier, None)) is not None:
                subaction(enzrxns, id_, content, last_content)
        else:
            if (action := actions.get(identifier, None)) is not None:
                action(enzrxns, id_, content)
                last_identifier = identifier
                last_content = content
    return enzrxns
