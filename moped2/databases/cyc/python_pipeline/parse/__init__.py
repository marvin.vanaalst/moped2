from __future__ import annotations

import logging
from pathlib import Path
from typing import Iterable

from ...data import ParseCompound, ParseEnzyme, ParseReaction, ParseResult
from .compounds import parse_compounds
from .enzymes import parse_enzymes
from .genes import parse_genes
from .proteins import parse_proteins
from .reactions import parse_reactions
from .sequences import parse_sequences

logger = logging.getLogger(__name__)


def _remove_top_comments(file: list[str]) -> list[str]:
    """Remove the metainformation from a pgdb file."""
    for i, line in enumerate(file):
        if line.startswith("UNIQUE-ID"):
            return file[i:]
    return file


def _read_file(path: Path) -> list[str]:
    try:
        with open(path, encoding="ISO-8859-14") as f:
            return f.readlines()
    except FileNotFoundError:
        return []


def _read_file_and_remove_comments(path: Path) -> list[str]:
    """Read the file and remove metainformation."""
    return _remove_top_comments(_read_file(path))


def _check_for_monomer(
    enzrxn: str,
    protein: str,
    monomers: Iterable[str],
    complexes: dict[str, set[str]],
    enzrxn_to_monomer: dict[str, set[str]],
) -> None:
    """Check complex tree until you arrive at monomers."""
    try:
        for subcomplex in complexes[protein]:
            if subcomplex in monomers:
                enzrxn_to_monomer.setdefault(enzrxn, set()).add(subcomplex)
            else:
                _check_for_monomer(enzrxn, subcomplex, monomers, complexes, enzrxn_to_monomer)
    except KeyError:
        pass


def _get_enzrnx_to_monomer_mapping(
    enzrxns: dict[str, ParseEnzyme],
    monomers: Iterable[str],
    complexes: dict[str, set[str]],
) -> dict[str, set[str]]:
    """Get mapping of enzyme reactions to monomers."""
    enzrxn_to_monomer: dict[str, set[str]] = {}
    for enzrxn, enzrxn_dict in enzrxns.items():
        protein = enzrxn_dict.enzyme
        if protein is not None:
            if protein in monomers:
                enzrxn_to_monomer.setdefault(enzrxn, set()).add(protein)
            else:
                _check_for_monomer(enzrxn, protein, monomers, complexes, enzrxn_to_monomer)
    return enzrxn_to_monomer


def _get_enzrnx_to_sequence_mapping(
    enzrxn_to_monomer: dict[str, set[str]], sequences: dict[str, str]
) -> dict[str, dict[str, str]]:
    """Get mapping of enzyme reactions to sequences."""
    enzrxn_to_sequence: dict[str, dict[str, str]] = {}
    for enzrxn, monomers in enzrxn_to_monomer.items():
        for monomer in monomers:
            try:
                sequence = sequences[monomer]
                enzrxn_to_sequence.setdefault(enzrxn, dict())[monomer] = sequence
            except KeyError:
                pass
    return enzrxn_to_sequence


def _map_reactions_to_sequences(
    reactions: dict[str, ParseReaction],
    enzrxn_to_monomer: dict[str, set[str]],
    enzrxn_to_seq: dict[str, dict[str, str]],
) -> None:
    """Get mapping of enzyme reactions to sequences."""
    for reaction in reactions.values():
        try:
            for enzrxn in reaction.enzymes:
                try:
                    reaction.sequences.update(enzrxn_to_seq[enzrxn])
                except KeyError:
                    pass
                try:
                    reaction.monomers.setdefault(enzrxn, set()).update(enzrxn_to_monomer[enzrxn])
                except KeyError:
                    pass
        except KeyError:
            pass


def _map_reactions_to_kinetic_parameters(
    reactions: dict[str, ParseReaction],
    enzrxns: dict[str, ParseEnzyme],
) -> None:
    """Get mapping of enzyme reactions to kinetic parameters."""
    for reaction in reactions.values():
        try:
            for enzrxn in reaction.enzymes:
                try:
                    enzyme = enzrxns[enzrxn]
                except KeyError:
                    pass
                else:
                    if bool(enzyme.kcat):
                        reaction.enzrxns.setdefault(enzyme.id, {}).setdefault("kcat", enzyme.kcat)
                    if bool(enzyme.km):
                        reaction.enzrxns.setdefault(enzyme.id, {}).setdefault("km", enzyme.km)
                    if bool(enzyme.vmax):
                        reaction.enzrxns.setdefault(enzyme.id, {}).setdefault("vmax", enzyme.vmax)
        except KeyError:
            pass


def parse(path: Path) -> ParseResult:
    """Parse the database."""
    compounds = parse_compounds(_read_file_and_remove_comments(path / "compounds.dat"))
    reactions = parse_reactions(_read_file_and_remove_comments(path / "reactions.dat"))
    genes = parse_genes(_read_file_and_remove_comments(path / "genes.dat"))
    enzrxns = parse_enzymes(_read_file_and_remove_comments(path / "enzrxns.dat"))
    monomers, complexes = parse_proteins(_read_file_and_remove_comments(path / "proteins.dat"))
    sequences = parse_sequences(iter(_read_file(path / "protseq.fsa")))
    enzrxn_to_monomer = _get_enzrnx_to_monomer_mapping(enzrxns, monomers, complexes)
    enzrxn_to_seq = _get_enzrnx_to_sequence_mapping(enzrxn_to_monomer, sequences)
    enzrxn_to_monomer = _get_enzrnx_to_monomer_mapping(enzrxns, monomers, complexes)
    enzrxn_to_seq = _get_enzrnx_to_sequence_mapping(enzrxn_to_monomer, sequences)
    _map_reactions_to_sequences(reactions, enzrxn_to_monomer, enzrxn_to_seq)
    _map_reactions_to_kinetic_parameters(reactions, enzrxns)
    return ParseResult(compounds, reactions, genes)
