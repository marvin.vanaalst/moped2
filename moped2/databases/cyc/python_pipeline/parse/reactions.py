from __future__ import annotations

import logging
from typing import Any, Callable, Iterable

from ...data import ParseReaction
from .shared import (
    MALFORMED_LINE_STARTS,
    _add_database_link,
    _add_type,
    _rename,
    _set_gibbs0,
    _set_name,
)


logger = logging.getLogger(__name__)


def _set_ec_number(reactions: dict[str, ParseReaction], id_: str, ec_number: str) -> None:
    reactions[id_].ec = ec_number


def _add_reaction_pathway(reactions: dict[str, ParseReaction], id_: str, pathway: str) -> None:
    reactions[id_].pathways.add(pathway)


def _add_reaction_enzyme(reactions: dict[str, ParseReaction], id_: str, enzyme: str) -> None:
    reactions[id_].enzymes.add(enzyme)


def _set_reaction_direction(reactions: dict[str, ParseReaction], id_: str, direction: str) -> None:
    reactions[id_].direction = direction
    if direction == "REVERSIBLE":
        reactions[id_].reversible = True
    else:
        reactions[id_].reversible = False


def _add_reaction_location(reactions: dict[str, ParseReaction], id_: str, location: str) -> None:
    location = location.replace("CCI-", "CCO-")
    if location.startswith("CCO-"):
        reactions[id_].locations.append(location)


def _set_substrate(
    reactions: dict[str, ParseReaction],
    id_: str,
    substrate: str,
) -> None:
    substrate = _rename(substrate) + "_c"
    reactions[id_].substrates[substrate] = -1
    reactions[id_].substrate_compartments[substrate] = "CCO-IN"


def _set_product(
    reactions: dict[str, ParseReaction],
    id_: str,
    product: str,
) -> None:
    product = _rename(product) + "_c"
    reactions[id_].products[product] = 1
    reactions[id_].product_compartments[product] = "CCO-IN"


def _set_substrate_coefficient(
    reactions: dict[str, ParseReaction],
    id_: str,
    coefficient: str,
    substrate: str,
) -> None:
    try:
        reactions[id_].substrates[_rename(substrate) + "_c"] = -float(coefficient)
    except ValueError:  # conversion failed
        pass


def _set_product_coefficient(
    reactions: dict[str, ParseReaction],
    id_: str,
    coefficient: str,
    product: str,
) -> None:
    try:
        reactions[id_].products[_rename(product) + "_c"] = float(coefficient)
    except ValueError:  # conversion failed
        pass


def _set_substrate_compartment(
    reactions: dict[str, ParseReaction],
    id_: str,
    compartment: str,
    substrate: str,
) -> None:
    substrate = _rename(substrate) + "_c"
    if compartment == "CCO-OUT":
        reactions[id_].substrate_compartments[substrate] = compartment
    elif compartment == "CCO-MIDDLE":
        reactions[id_].substrate_compartments[substrate] = "CCO-OUT"
    else:
        # reactions[id_].product_compartments[substrate] = "CCO-OUT"
        logger.info(f"Unknown compartment {compartment}")


def _set_product_compartment(
    reactions: dict[str, ParseReaction],
    id_: str,
    compartment: str,
    product: str,
) -> None:
    product = _rename(product) + "_c"
    if compartment == "CCO-OUT":
        reactions[id_].product_compartments[product] = compartment
    elif compartment == "CCO-MIDDLE":
        reactions[id_].product_compartments[product] = "CCO-OUT"
    else:
        # reactions[id_].product_compartments[product] = "CCO-OUT"
        logger.info(f"Unknown compartment {compartment}")


def parse_reactions(file: Iterable[str]) -> dict[str, ParseReaction]:
    actions: dict[str, Callable[[Any, Any, Any], None]] = {
        "TYPES": _add_type,
        "COMMON-NAME": _set_name,
        "DBLINKS": _add_database_link,
        "EC-NUMBER": _set_ec_number,
        "ENZYMATIC-REACTION": _add_reaction_enzyme,
        "GIBBS-0": _set_gibbs0,
        "IN-PATHWAY": _add_reaction_pathway,
        "LEFT": _set_substrate,
        "REACTION-DIRECTION": _set_reaction_direction,
        "RIGHT": _set_product,
        "RXN-LOCATIONS": _add_reaction_location,
    }

    sub_actions: dict[str, dict[str, Callable[[Any, Any, Any, Any], None]]] = {
        "^COMPARTMENT": {
            "LEFT": _set_substrate_compartment,
            "RIGHT": _set_product_compartment,
        },
        "^COEFFICIENT": {
            "LEFT": _set_substrate_coefficient,
            "RIGHT": _set_product_coefficient,
        },
    }

    id_ = ""
    reactions = {}
    last_identifier = ""
    last_content = ""
    for line in file:
        if any(line.startswith(i) for i in MALFORMED_LINE_STARTS):
            continue
        try:
            identifier, content = line.rstrip().split(" - ", maxsplit=1)
        except ValueError:
            logger.info(f"Malformed line in reactions.dat: {line}")
            continue

        if identifier == "UNIQUE-ID":
            id_ = content
            reactions[id_] = ParseReaction(id=id_, base_id=id_)
        elif identifier.startswith("^"):
            if (subaction := sub_actions.get(identifier, {}).get(last_identifier, None)) is not None:
                subaction(reactions, id_, content, last_content)
        else:
            if (action := actions.get(identifier, None)) is not None:
                action(reactions, id_, content)
                last_identifier = identifier
                last_content = content

    return reactions
