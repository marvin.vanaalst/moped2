# from cycparser import RawKineticData, RawMonomer


# def get_monomers_from_raw_monomers(
#     raw_monomers: dict[str, dict[str, RawMonomer]]
# ) -> dict[str, dict[str, Monomer]]:
#     return {
#         k: {k2: Monomer(id=k2, gene=v.gene, database_links=v.database_links)}
#         for k, d in raw_monomers.items()
#         for k2, v in d.items()
#     }


# def get_kinetic_data_from_raw(raw_kinetic_data: dict[str, RawKineticData]) -> dict[str, KineticData]:
#     return {k: KineticData(km=v.km, kcat=v.kcat) for k, v in raw_kinetic_data.items()}


# def parse_and_repair(self) -> tuple[dict[str, ParseCompound], dict[str, ParseReaction]]:
#     return self.repair(*self.parse())


# def parse(
#     self,
# ) -> tuple[dict[str, ParseCompound], dict[str, list[str]], dict[str, ParseReaction], dict[str, Monomer]]:
#     return Parser(
#         pgdb_path=self.pgdb_path,
#         compartment_map=self.compartment_map,
#         type_map=self.type_map,
#         parse_sequences=self.parse_sequences,
#     ).parse()


# def rust_pipeline(
#     pgdb_path: Path,
#     type_map: dict[str, str],
#     manual_additions: dict[str, ParseCompound],
#     compartment_map: dict[str, str],
#     compartment_suffixes: dict[str, str],
# ) -> Cyc:
#     def get_monomers_from_raw_monomers(
#         raw_monomers: dict[str, dict[str, RawMonomer]]
#     ) -> dict[str, dict[str, Monomer]]:
#         return {
#             k: {k2: Monomer(id=k2, gene=v.gene, database_links=v.database_links)}
#             for k, d in raw_monomers.items()
#             for k2, v in d.items()
#         }
