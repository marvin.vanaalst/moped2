from __future__ import annotations

__all__ = ["python_pipeline", "defaults"]

from pathlib import Path
from typing import Optional, cast

from . import python_pipeline, defaults

from ...core.compound import Compound
from ...core.model import Model
from ...core.reaction import Reaction
from .data import ParseCompound, Pipeline


# from .rust_pipeline import rust_pipeline


def _move_reaction_to_other_compartment(model: Model, reaction: Reaction) -> None:
    reaction.id = cast(str, reaction.base_id)
    if (count := reaction._var) is not None:
        reaction.id += f"__var__{count}"
    new_compartments = tuple(
        sorted({cast(str, model.compounds[i].compartment) for i in reaction.stoichiometries})
    )
    if len(new_compartments) == 1:
        compartment = new_compartments[0]
        reaction.compartment = compartment
        reaction.transmembrane = False
        reaction.id += model._add_compartment_suffix(object_id="", compartment_id=compartment)
    else:
        reaction.compartment = new_compartments
        reaction.transmembrane = True
        for compartment_id in reaction.compartment:
            reaction.id += model._add_compartment_suffix(object_id="", compartment_id=compartment_id)
    model.add_reaction(reaction=reaction)


def _fix_light_reaction_mass_balance(model: Model) -> None:
    """This one is really weird.
    Memote requires us to give light a mass and then add another compound,
    such that we can mass-balance the reactions. Yes.
    """
    if (cpd := model.compounds.get("Light_c")) is not None:
        if "EXTRACELLULAR" not in model.compartments:  # can happen in rare cases
            model.compartments["EXTRACELLULAR"] = "e"
        cpd.formula = {"Photon": 1}
        model.add_compound(
            Compound(
                base_id="Light_used",
                compartment="EXTRACELLULAR",
                id="Light_used_e",
                charge=0,
                formula={"Photon": 1},
            )
        )
        model.add_efflux("Light_used_e", "EXTRACELLULAR")
        for reaction in model.reactions.values():
            if "Light_c" in reaction.stoichiometries:
                reaction.stoichiometries["Light_used_e"] = -1 * reaction.stoichiometries["Light_c"]


def _fix_periplasm_proton_gradient(model: Model) -> None:
    """Set reactions that do not match proton gradient criteria to irreversible.

    The criteria are the following:
    - All reactions of the following types are always kept
        TR-13 (Transport Energized by Phosphoanhydride-Bond Hydrolysis)
        TR-15 (Transport Energized by Decarboxylation)
        Membrane-Protein-Modification-Reactions
        Electron-Transfer-Reactions
    - If a reaction otherwise transports protons inside of the periplasm it is
        usually of the type
        TR-12 (Transport Energized by the Membrane Electrochemical Gradient)
    Keeping those reactions leads to thermodynamically infeasible loops, therefore
    they are reverted to a way that they only transport protons to the cytosol
    and are irreversible.
    """
    if "PERIPLASM" in model.get_model_compartment_ids():
        periplasm_recs = [
            model.reactions[i] for i in model.get_transport_reactions(compartment_id="PERIPLASM")
        ]
        proton_translocators = {i.id for i in periplasm_recs if "PROTON_p" in i.stoichiometries}
        try:
            tr13 = model.get_reactions_of_type(reaction_type="TR-13")
        except KeyError:
            tr13 = set()
        try:
            tr15 = model.get_reactions_of_type(reaction_type="TR-15")
        except KeyError:
            tr15 = set()
        try:
            mpmr = model.get_reactions_of_type(reaction_type="Membrane-Protein-Modification-Reactions")
        except KeyError:
            mpmr = set()
        try:
            etr = model.get_reactions_of_type(reaction_type="Electron-Transfer-Reactions")
        except KeyError:
            etr = set()
        to_fix = proton_translocators.difference(tr13 | tr15 | etr | mpmr)
        for reaction_id in to_fix:
            reaction = model.reactions[reaction_id]
            if reaction.stoichiometries["PROTON_p"] > 0:
                reaction.reverse_stoichiometry()
            reaction.make_irreversible()


def _fix_move_electron_transport_cofactors_to_cytosol(model: Model) -> None:
    """Move all periplasmatic electron transport cofactors into cytosol.

    This is done to keep the connectivity of the network.
    """
    cofactors = {
        "Reduced-ferredoxins_p": "Reduced-ferredoxins_c",
        "Oxidized-ferredoxins_p": "Oxidized-ferredoxins_c",
        "Cytochromes-C-Reduced_p": "Cytochromes-C-Reduced_c",
        "Cytochromes-C-Oxidized_p": "Cytochromes-C-Oxidized_c",
        "NADPH_p": "NADPH_c",
        "NADP_p": "NADP_c",
        "NAD_p": "NAD_c",
        "ATP_p": "ATP_c",
        "ADP_p": "ADP_c",
    }

    try:
        etr_reactions = model.get_reactions_of_type(reaction_type="Electron-Transfer-Reactions")
    except KeyError:
        pass
    else:
        for reaction_id in tuple(etr_reactions):
            reaction = model.reactions[reaction_id].copy()
            if "PERIPLASM" in cast(str, reaction.compartment):
                changed_cpds = False
                for compound_id in tuple(reaction.stoichiometries):
                    if compound_id in cofactors:
                        reaction.stoichiometries[cofactors[compound_id]] = reaction.stoichiometries.pop(
                            compound_id
                        )
                        changed_cpds = True
                if changed_cpds:
                    model.remove_reaction(
                        reaction_id=reaction_id,
                        remove_empty_references=False,
                    )
                    _move_reaction_to_other_compartment(model, reaction)


def _fix_repair_photosynthesis_reactions(model: Model) -> None:
    """Switch the photosynthesis reactions proton compartments.

    The way the photosynthesis reactions are currently annotated and parsed,
    they will transport protons out of the periplasm, while they are
    actually doing the opposite.
    """
    try:
        for reaction_id in list(model.pathways["PWY-101"]):
            reaction = model.reactions[reaction_id].copy()
            if reaction.transmembrane:
                model.remove_reaction(reaction_id, remove_empty_references=False)
                st = reaction.stoichiometries
                in_compartment, out_compartment = cast(tuple[str, str], reaction.compartment)
                in_proton_name = model._add_compartment_suffix(
                    object_id="PROTON", compartment_id=in_compartment
                )
                out_proton_name = model._add_compartment_suffix(
                    object_id="PROTON", compartment_id=out_compartment
                )
                try:
                    in_protons = st.pop(in_proton_name)
                    in_error = False
                except KeyError:
                    in_error = True
                try:
                    out_protons = st.pop(out_proton_name)
                    out_error = False
                except KeyError:
                    out_error = True
                if not in_error:
                    st[out_proton_name] = in_protons  # type: ignore
                if not out_error:
                    st[in_proton_name] = out_protons  # type: ignore
                _move_reaction_to_other_compartment(model, reaction)
    except KeyError:
        pass


def create_model_from_pgdb(
    pgdb_path: Path | str,
    type_map: Optional[dict[str, str]] = None,
    manual_additions: Optional[dict[str, ParseCompound]] = None,
    compartment_map: Optional[dict[str, str]] = None,
    compartment_suffixes: Optional[dict[str, str]] = None,
    minimal_seed: Optional[set[str]] = None,
    cofactor_pairs: Optional[dict[str, str]] = None,
    fix_move_electron_transport_cofactors_to_cytosol: bool = True,
    fix_repair_photosynthesis_reactions: bool = True,
    fix_periplasm_proton_gradient: bool = True,
    fix_light_reaction_mass_balance: bool = True,
    remove_unused_compounds: bool = True,
    pipeline: Optional[Pipeline] = None,
) -> Model:
    pgdb_path = Path(pgdb_path)
    if type_map is None:
        type_map = defaults.TYPE_MAP
    if manual_additions is None:
        manual_additions = defaults.MANUAL_ADDITIONS
    if compartment_map is None:
        compartment_map = defaults.COMPARTMENT_MAP
    if compartment_suffixes is None:
        compartment_suffixes = defaults.COMPARTMENT_SUFFIXES
    if minimal_seed is None:
        minimal_seed = defaults.MINIMAL_SEED
    if cofactor_pairs is None:
        cofactor_pairs = defaults.COFACTOR_PAIRS
    if pipeline is None:
        pipeline = python_pipeline.pipeline  # type: ignore

    cyc = pipeline(pgdb_path, type_map, manual_additions, compartment_map, compartment_suffixes)  # type: ignore
    model = cyc.to_model(compartment_suffixes, minimal_seed, cofactor_pairs)
    if fix_move_electron_transport_cofactors_to_cytosol:
        _fix_move_electron_transport_cofactors_to_cytosol(model)
    if fix_repair_photosynthesis_reactions:
        _fix_repair_photosynthesis_reactions(model)
    if fix_periplasm_proton_gradient:
        _fix_periplasm_proton_gradient(model)
    if fix_light_reaction_mass_balance:
        _fix_light_reaction_mass_balance(model)
    if remove_unused_compounds:
        model.remove_unused_compounds()
    return model
