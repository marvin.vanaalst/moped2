from __future__ import annotations

from dataclasses import dataclass, field
from pathlib import Path
from typing import Callable, Dict

from ...core.compound import Compound
from ...core.model import Model
from ...core.reaction import KineticData, Monomer, Reaction


@dataclass
class ParseGene:
    id: str
    product: str | None = None
    database_links: dict[str, set[str]] = field(default_factory=dict)


@dataclass
class ParseCompound:
    id: str
    base_id: str
    charge: int = 0
    compartment: str = "CYTOSOL"
    smiles: str | None = None
    name: str | None = None
    gibbs0: float | None = None
    types: list[str] = field(default_factory=list)
    formula: dict[str, float] = field(default_factory=dict)
    database_links: dict[str, set[str]] = field(default_factory=dict)


@dataclass
class ParseReaction:
    id: str
    base_id: str
    bounds: tuple[float, float] = (0, 0)
    name: str | None = None
    ec: str | None = None
    gibbs0: float | None = None
    direction: str = "LEFT-TO-RIGHT"
    reversible: bool = False
    transmembrane: bool = False
    substrates: dict[str, float] = field(default_factory=dict)
    substrate_compartments: dict[str, str] = field(default_factory=dict)
    products: dict[str, float] = field(default_factory=dict)
    product_compartments: dict[str, str] = field(default_factory=dict)
    types: list[str] = field(default_factory=list)
    locations: list[str] = field(default_factory=list)
    pathways: set[str] = field(default_factory=set)
    enzymes: set[str] = field(default_factory=set)
    database_links: dict[str, set[str]] = field(default_factory=dict)
    _var: int | None = None
    # Later additions
    monomers: dict[str, set[str]] = field(default_factory=dict)
    monomers_annotated: dict[str, dict[str, Monomer]] = field(default_factory=dict)
    sequences: dict[str, str] = field(default_factory=dict)
    enzrxns: dict[str, dict[str, dict[str, float]]] = field(default_factory=dict)
    kinetic_data: dict[str, KineticData] = field(default_factory=dict)
    compartment: str | tuple[str, str] | None = None
    stoichiometries: dict[str, float] = field(default_factory=dict)


@dataclass
class ParseEnzyme:
    id: str
    enzyme: str | None = None
    kcat: dict[str, float] = field(default_factory=dict)
    km: dict[str, float] = field(default_factory=dict)
    vmax: dict[str, float] = field(default_factory=dict)


@dataclass
class ParseResult:
    compounds: dict[str, ParseCompound]
    reactions: dict[str, ParseReaction]
    genes: dict[str, Monomer]


@dataclass
class Cyc:
    compounds: dict[str, ParseCompound]
    reactions: dict[str, ParseReaction]

    def to_model(
        self,
        compartment_suffixes: dict[str, str],
        minimal_seed: set[str],
        cofactor_pairs: dict[str, str],
    ) -> Model:
        compounds = [
            Compound(
                base_id=v.base_id,
                compartment=v.compartment,
                formula=v.formula,
                charge=v.charge,
                name=v.name,
                gibbs0=v.gibbs0,
                smiles=v.smiles,
                database_links=v.database_links,
                types=v.types,
                id=v.id,
            )
            for v in self.compounds.values()
        ]
        reactions = [
            Reaction(
                base_id=v.base_id,
                id=v.id,
                stoichiometries=v.stoichiometries,
                compartment=v.compartment,
                name=v.name,
                bounds=v.bounds,
                gibbs0=v.gibbs0,
                ec=v.ec,
                types=v.types,
                pathways=v.pathways,
                sequences=v.sequences,
                monomers=v.monomers_annotated,
                kinetic_data=v.kinetic_data,
                database_links=v.database_links,
                transmembrane=v.transmembrane,
                _var=v._var,
            )
            for v in self.reactions.values()
        ]
        used_compartments: set[str] = {i.compartment for i in compounds if i.compartment is not None}
        compartments: dict[str, str] = {i: compartment_suffixes[i] for i in used_compartments}
        return Model(
            compounds=compounds,
            reactions=reactions,
            compartments=compartments,
            minimal_seed=minimal_seed,
            cofactor_pairs=cofactor_pairs,
            objective=None,
            name=None,
        )


Pipeline = Callable[[Path, Dict[str, str], Dict[str, ParseCompound], Dict[str, str], Dict[str, str]], Cyc]
